-- sqlite3 -cmd "pragma foreign_keys = ON" -cmd ".read init.sql" -cmd ".read fixtures.sql"

-- Federation & Identity -------------------------------------------------------

create table "providers" (
    "id"        integer primary key,
    "name"      text    not null,
    "url"       text    not null,

    constraint "providers__name_length"
         check (length("name") > 0 and length("name") <= 512)
);

create table "identities" (
    "id"        integer primary key,
    "name"      text    not null,

    constraint "identities__name_length"
         check (length("name") > 0 and length("name") <= 512),

    unique ("name")
);

-- Tasks & Doings? -------------------------------------------------------------

create table "notes" (
    "id"        integer     primary key,
    "uuid"      blob        not null default (uuid_generate_v4()),
    "text"      text        not null,

    constraint "notes__text_length" check (length("text") > 0 and length("text") <= 1024)
);

create unique index "notes__idx__uuid" on "notes" ("uuid");

-- An event at a single moment, (like a grafana annotation).
create table "event" (
    "id"        integer     primary key,
    "uuid"      blob        not null default (uuid_generate_v4()),
    "when"      timestamptz not null,
    "note"      integer     not null references "notes"
);

create unique index "event__idx__uuid" on "event" ("uuid");

create table "event_participants" (
    "id"        integer primary key,
    "event"     integer not null references "events",
    "identity"  integer not null references "identities",
    "role"      text    not null,

    constraint "event_participants__role_length"
         check (length("role") > 0 and length("role") <= 128)
);

create unique index "event_participants__idx__event_identity" on "event_participants" ("event", "identity");
create        index "event_participants__idx__identity" on "event_participants" ("identity");

-- A duration attached to a note
create table "doings" (
    "id"        integer     primary key,
    "uuid"      blob        not null default (uuid_generate_v4()),
    "start"     timestamptz not null,
    "end"       timestamptz,
    "note"      integer     not null references "notes",

    constraint "doings__ends_after_start" check ("end" > "start")
);

create unique index "doings__idx__uuid" on "doings" ("uuid");

      create trigger "doings_cannot_overlap_on_insert"
    before insert on "doings"
        for each row
                when "new"."end" is not null
    begin
          select raise(abort, "start/end overlap")
    where exists
      ( select 1
          from "doings" as "d"
         where "d"."start" <= "new"."start" and "d"."end"   > "new"."start"
            or "d"."end"   >  "new"."start" and "d"."start" < "new"."end" );
    end;

      create trigger "doings_cannot_overlap_on_update"
    before update of "start", "end" on "doings"
        for each row
    begin
          select raise(abort, "start/end overlap")
    where exists
      ( select 1
          from "doings" as "d"
         where "d"."id" != "new"."id"
           and ("d"."start" <= "new"."start" and "d"."end"   > "new"."start"
             or "d"."end"   >  "new"."start" and "d"."start" < "new"."end") );
    end;

create table "doing_participants" (
    "id"        integer primary key,
    "doing"     integer not null references "doings",
    "identity"  integer not null references "identities",
    "role"      text    not null,

    constraint "doing_participants__role_length"
         check (length("role") > 0 and length("role") <= 128)
);

create unique index "doing_participants__idx__doing_identity" on "doing_participants" ("doing", "identity");
create        index "doing_participants__idx__identity" on "doing_participants" ("identity");

-- Tagging ---------------------------------------------------------------------

create table "tags" (
    "id"        integer     primary key,
    "note"      integer     not null references "notes" ("id")
                                     on delete cascade,
    -- location in string on the doing/whatever?
    -- should be null if this tag is from a rule ...
    "start"     integer,
    "end"       integer,

    "key"       text        not null collate "binary",
    "value"     blob        not null default '',

    constraint "tags__key_characters" check ("key" regexp '^[a-z0-9-]*$'),
    constraint "tags__ends_after_start" check ("end" > "start")
);

create index "tags__idx__note" on "tags" ("note");
create index "tags__idx__key" on "tags" ("key");

create table "rules" (
    "id"        integer     primary key,
    "uuid"      blob        not null default (uuid_generate_v4()),

    "match_key" text        not null,
    -- if match-value is null, it matches every value
    "match_value" blob,

    "key"       text        not null collate "binary",
    "value"     blob        not null default '',

    constraint "rules__key_characters" check ("key" regexp '^[a-z0-9-]*$'),
    constraint "rules__match_key_characters" check ("match_key" regexp '^[a-z0-9-]*$')
);

      create trigger "create_tags_from_rules"
    before insert on "tags"
        for each row
                when "new"."start" is not null
                 and "new"."end" is not null
    begin
        insert into "tags" ("note", "start", "end", "key", "value")
             select "new"."note", null, null, "r"."key", "r"."value"
               from "rules" "r"
              where "r"."match_key" = "new"."key"
                                  -- match any or match exact ...
                and ("r"."match_value" is null or "r"."match_value" = "new"."value")
            ;
    end;


-- materialized view of doing tags ... maintained by triggers
create table "doing_tags" (
    "id"        integer     primary key,
    "doing"     integer     not null references "doings" ("id")
                                     on delete cascade,
    "tag"       integer     not null references "tags" ("id")
                                     on delete cascade
);


      create trigger "create_doing_tags_from_tags"
     after insert on "tags"
        for each row
    begin
         insert into "doing_tags" ("doing", "tag")
              select "d"."id", "new"."id"
                from "doings" "d"
               where "d"."note" = "new"."note"
             ;
    end;

      create trigger "create_doing_tags_from_new_doing"
     after insert on "doings"
        for each row
    begin
         insert into "doing_tags" ("doing", "tag")
              select "new"."id", "t"."id"
                from "tags" "t"
               where "t"."note" = "new"."note"
             ;
    end;

      create trigger "create_doing_tags_from_change_doing_note"
     after update on "doings"
        for each row
                when "new"."note" != "old"."note"
    begin
         delete from "doing_tags"
               where "doing" = "old"."id"
            ;
         insert into "doing_tags" ("doing", "tag")
              select "new"."id", "t"."id"
                from "tags" "t"
               where "t"."note" = "new"."note"
             ;
    end;
