//! Important Impetuous data models?
//!
//! Uses [impress] to build silly dispatch stuff for doing find or mutate stuff.
pub use crate::time::DateTime;
use lazy_static::lazy_static;
use rusqlite::types::{FromSql, FromSqlResult, ToSql, ToSqlOutput, ValueRef};
pub use uuid::Uuid;

use crate::im::Impetuous;
use impress::{dispatch, maybe::maybe, orm};
// macros!
use impress::{has_pk, has_sk, model, mutable, orm_interaction};

pub struct Interaction<'l> {
    pub im: &'l Impetuous,
    pub orm: dispatch::Interaction<'l>,
}

impl<'l> Interaction<'l> {
    /// create a new interaction from a database connection, uses the root slog logger
    pub fn new(im: &'l Impetuous, db: &'l mut rusqlite::Connection) -> Self {
        Interaction {
            im,
            orm: dispatch::Interaction::new(db, crate::logging::please().clone()),
        }
    }
}

impl<'l> dispatch::MapErr for Interaction<'l> {
    fn map_err(e: rusqlite::Error) -> dispatch::Error {
        if let rusqlite::Error::SqliteFailure(_, Some(msg)) = &e {
            debug!(crate::logging::please(), "trying to map error: {:?}", e);
            if let Some(why) = hint_for_sqlite_constraint_violation(msg) {
                return dispatch::Error::BadRequest { why, index: None };
            }
        }
        return e.into();
    }
}

impl<'l> AsMut<dispatch::Interaction<'l>> for Interaction<'l> {
    fn as_mut(&mut self) -> &mut dispatch::Interaction<'l> {
        &mut self.orm
    }
}

impl<'l> dispatch::IntoParts<dispatch::Interaction<'l>> for Interaction<'l> {
    type Lh = &'l Impetuous;

    fn into_parts(self) -> (Self::Lh, dispatch::Interaction<'l>) {
        let Interaction { im, orm } = self;
        (im, orm)
    }
}

impl<'l> Into<dispatch::Interaction<'l>> for Interaction<'l> {
    fn into(self) -> dispatch::Interaction<'l> {
        self.orm
    }
}

model! {
    notes(id: i64) Note {
        uuid: Uuid,
        text: String,
        @link-back doings: Vec<Doing> <= doings(note),
        @link-back events: Vec<Event> <= events(note),
        @link-back tags: Vec<Tag> <= tags(note),
    }

    @log crate::logging::please().clone()
}

has_sk!(Note uuid: Uuid);

model! {
    events(id: i64) Event {
        uuid: Uuid,
        when: DateTime,
        @link note: Box<Note> => notes(id),
    }

    @log crate::logging::please().clone()
}

has_sk!(Event uuid: Uuid);

model! {
    doings(id: i64) Doing {
        uuid: Uuid,
        start: DateTime,
        end: Option<DateTime>,
        @link note: Box<Note> => notes(id),
        @link-back postings: Vec<Posting> <= postings(doing),
    }

    @log crate::logging::please().clone()
}

has_sk!(Doing uuid: Uuid);

model! {
    tags(id: i64) Tag {
        key: String,
        value: String,
        start: Option<u16>,
        end: Option<u16>,
        @link note: Box<Note> => notes(id),
    }

    @log crate::logging::please().clone()
}

model! {
    doing_tags(id: i64) DoingTag {
        @link doing: Box<Doing> => doings(id),
        @link tag: Box<Tag> => tags(id),
    }

    @log crate::logging::please().clone()
}

model! {
    rules(id: i64) Rule {
        uuid: Uuid,
        match_key: String,
        match_value: Option<String>,
        key: String,
        value: String,
    }

    @log crate::logging::please().clone()
}

has_sk!(Rule uuid: Uuid);

model! {
    postings(id: i64) Posting {
        uuid: Uuid,
        source: String,
        entity: String,
        created_at: DateTime,
        status: PostingStatus,
        detail: String,
        @link doing: Box<Doing> => doings(id),
    }

    @log crate::logging::please().clone()
}

has_sk!(Posting uuid: Uuid);

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone, Copy)]
pub enum PostingStatus {
    #[serde(rename = "unknown")]
    Unknown(i64),
    #[serde(rename = "new")]
    New, // 0
    #[serde(rename = "pending")]
    Pending, // 1
    #[serde(rename = "success")]
    Success, // 2
    #[serde(rename = "failed")]
    Failed, // -1
}

impl Into<i64> for PostingStatus {
    fn into(self) -> i64 {
        match self {
            PostingStatus::Unknown(i) => i,
            PostingStatus::New => 0,
            PostingStatus::Pending => 1,
            PostingStatus::Success => 2,
            PostingStatus::Failed => -1,
        }
    }
}

impl From<i64> for PostingStatus {
    fn from(i: i64) -> PostingStatus {
        match i {
            0 => PostingStatus::New,
            1 => PostingStatus::Pending,
            2 => PostingStatus::Success,
            -1 => PostingStatus::Failed,
            _ => PostingStatus::Unknown(i),
        }
    }
}

impl ToSql for PostingStatus {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput> {
        let v = rusqlite::types::Value::Integer((*self).into());
        Ok(ToSqlOutput::Owned(v))
    }
}

impl FromSql for PostingStatus {
    fn column_result(value: ValueRef) -> FromSqlResult<Self> {
        value.as_i64().map(PostingStatus::from)
    }
}

impl Doing {
    /// new doing with a new note
    pub fn new<S: Into<String>>(start: DateTime, end: Option<DateTime>, text: S) -> Self {
        Doing {
            start: maybe(start),
            end: maybe(end),
            note: maybe(Box::new(Note {
                text: maybe(text),
                ..Note::default()
            })),
            ..Doing::default()
        }
    }

    pub fn is_in_progress(&self) -> Option<bool> {
        self.end.map(|e| e.is_none())
    }

    pub fn duration(&self) -> Option<chrono::Duration> {
        Some(*(*self.end)?? - *(*self.start)?) // lol
    }

    pub fn duration_or_until(&self, fallback: DateTime) -> Option<chrono::Duration> {
        Some(*(*self.end)?.unwrap_or(fallback) - *(*self.start)?) // lol
    }

    pub fn start_date(&self) -> Option<chrono::Date<chrono::Local>> {
        self.start.map(|dt| dt.with_timezone(&chrono::Local).date())
    }

    /// return the duration in minutes, rounded randomly ...
    pub fn duration_minutes(&self) -> Option<i64> {
        self.duration().map(|d| {
            use rand::Rng;
            let ts = d.num_seconds();
            let (m, s) = (ts / 60, ts % 60);
            if s <= rand::thread_rng().gen_range(0, 60) {
                m
            } else {
                m + 1
            }
        })
    }

    /// Return `since - start`, will be negative if since is earlier than start
    pub fn started_since<Tz: chrono::TimeZone>(
        &self,
        since: chrono::DateTime<Tz>,
    ) -> Option<chrono::Duration> {
        self.start
            .map(|s| s.into_chrono())
            .map(|start| since.signed_duration_since(start))
    }

    pub fn comment(&self) -> Option<&str> {
        self.note
            .as_ref()
            .and_then(|n| n.text.as_ref().map(|s| s.as_str()))
    }
}

impl Tag {
    fn new<S0, S1>(start: u16, end: u16, key: S0, value: S1) -> Self
    where
        S0: Into<String>,
        S1: Into<String>,
    {
        Tag {
            start: maybe(Some(start)),
            end: maybe(Some(end)),
            key: maybe(key.into()),
            value: maybe(value.into()),
            ..Tag::default()
        }
    }
}

impl Posting {
    pub fn pending<I: Into<String>>(detail: I) -> Self {
        Posting {
            status: maybe(PostingStatus::Pending),
            detail: maybe(detail),
            ..Posting::default()
        }
    }

    pub fn failed<I: Into<String>>(detail: I) -> Self {
        Posting {
            status: maybe(PostingStatus::Failed),
            detail: maybe(detail),
            ..Posting::default()
        }
    }

    pub fn success<I: Into<String>>(detail: I) -> Self {
        Posting {
            status: maybe(PostingStatus::Success),
            detail: maybe(detail),
            ..Posting::default()
        }
    }
}

orm_interaction! {
    @on Interaction ...

    Note {
        uuid: Uuid,
        text: String,
        @link tags -> Tag,
        @link doings -> Doing,
        @link events -> Event,
        @policy (|int: &mut Interaction, q: &mut orm::Q<Note>| {
            if orm::IsEmpty::is_empty(&q.get) && orm::IsEmpty::is_empty(&q.links) {
                q.get.text();
            }
        }),
    }

    Tag {
        key: String,
        value: String,
        start: Option<u16>,
        end: Option<u16>,
        @link note -> Note,
        // what the fuck this is awful
        @default-sort (|_, e: &mut <Tag as orm::Modeled>::FieldExpr| e.start().asc()),
        @policy (|int: &mut Interaction, q: &mut orm::Q<Tag>| {
            if orm::IsEmpty::is_empty(&q.get) && orm::IsEmpty::is_empty(&q.links) {
                q.get.key().value().start().end();
            }
        }),
    }

    Rule {
        match_key: String,
        match_value: Option<String>,
        key: String,
        value: String,
    }

    Event {
        uuid: Uuid,
        when: DateTime,
        @link note -> Note,
        @default-sort (|_, e: &mut <Event as orm::Modeled>::FieldExpr| e.when().desc()),
    }

    Doing {
        uuid: Uuid,
        start: DateTime,
        end: Option<DateTime>,
        @link note -> Note,
        @link postings -> Posting,
        @policy (|int: &mut Interaction, q: &mut orm::Q<Doing>| {
            if orm::IsEmpty::is_empty(&q.get) && orm::IsEmpty::is_empty(&q.links) {
                q.get.uuid().start().end();
                <Doing as orm::Modeled>::LinkSet::note(q).get.text();
            }
            if q.order_by.is_empty() {
                q.order_by(|e| e.start().desc());
            }
        }),
    }

    DoingTag {
        @link doing -> Doing,
        @link tag -> Tag,
    }

    Posting {
        uuid: Uuid,
        source: String,
        entity: String,
        created_at: DateTime,
        status: PostingStatus,
        detail: String,
        @link doing -> Doing,
        // default shape ...
        @policy (|int: &mut Interaction, q: &mut orm::Q<Posting>| {
            if orm::IsEmpty::is_empty(&q.get) && orm::IsEmpty::is_empty(&q.links) {
                q.get.source().entity().status().detail();
            }
        }),
    }
}

mutable! {
    @on Interaction ...

    Note as "notes" {
        text,
        @also RefreshTags,
        @also RefreshPostings,
    }

    Rule as "rules" { match_key, match_value, key, value, }

    Event as "events" { when, @link note, }

    Doing as "doings" { start, end, @link note, @also RefreshPostings, }

    Posting as "postings" { status, detail, }
}

/// For each Doing comment that we modify, we parse the comment and look for tags.
/// Delete the old tags if applicable. This makes no attempt to match new/old tags because they
/// have no surrogate keys, nobody keys to them (yet), and because I'm lazy.
struct RefreshTags;

impl<'l> dispatch::mutable::Also<'l, Interaction<'l>, Note> for RefreshTags {
    fn after_change(
        &mut self,
        im: &&'l Impetuous,
        tx: &rusqlite::Transaction,
        pk: i64,
        change: dispatch::Change<&Note>,
    ) -> dispatch::Result<()> {
        use impress::orm::{Modeled, SeqAliasing};
        use impress::sql::{self, BuildSql};

        let new_text = change.new.and_then(|n| n.text.as_ref());
        let old_text = change.old.and_then(|n| n.text.as_ref());

        // Do nothing if we have no new record or if the text is unchanged.
        let text = match (new_text, old_text) {
            (new, old) if new == old => return Ok(()),
            (None, _) => return Ok(()),
            (Some(text), _) => text,
        };

        if old_text.is_some() {
            // Delete all the tags on this note.
            // This should delete the ones created by rules as well ... they'll be recreated by a
            // super sneaky trigger.
            let mut aliasing = SeqAliasing::default();
            let table = aliasing.alias(Tag::table_name());
            let expr = Tag::field_expr(table.clone());
            let filter = expr.note().eq(orm::BIND);

            let sql = sql::Delete::default_tuples()
                .table(table)
                .filter(filter)
                .as_sql();

            if let Some(log) = Tag::logger() {
                debug!(log, "RefreshTags {} ... {}", sql, pk);
            }

            tx.execute(&sql, rusqlite::params![&pk])?;
        }

        for mut tag in tags_in_text(text) {
            tag.note = maybe(Box::new(Note {
                id: maybe(pk),
                ..Default::default()
            }));
            tag.insert(&tx)?;
        }

        Ok(())
    }
}

/// when a doing is created or its associated note changes or a doing's note content changes, then
/// then the view of doing -> note -> tags changes, and that's where postings come from, so they
/// have to be recalculated I guess
struct RefreshPostings;

impl RefreshPostings {
    fn refresh(
        im: &Impetuous,
        tx: &rusqlite::Transaction,
        q: orm::Q<DoingTag>,
    ) -> dispatch::Result<()> {
        use impress::{
            orm::Modeled,
            sql::{self, BuildSql},
        };

        let doing_tags = q.list(&tx)?;
        doing_tags
            .iter()
            .map(|dt| {
                let (dt, key, value, doing): (i64, &str, &str, i64) = match (
                    *dt.id,
                    dt.tag.as_ref().and_then(|t| t.key.as_ref()),
                    dt.tag.as_ref().and_then(|t| t.value.as_ref()),
                    dt.doing.as_ref().and_then(|d| *d.id),
                ) {
                    (Some(dt), Some(key), Some(value), Some(doing)) => (dt, key, value, doing),
                    _ => panic!("I lack critical information {:?}", dt),
                };

                im.config()
                    .integrations
                    .iter()
                    .filter(move |(_, i)| &i.tag == key)
                    .map(move |(_, i)| (dt, &i.name, value, PostingStatus::New, doing))
            })
            .flatten()
            .map(|(dt, source, key, value, doing)| {
                let expr = sql::Values::one(sql::ExprTuple::from((
                    sql::bind::<()>(),
                    sql::bind::<()>(),
                    sql::bind::<()>(),
                    sql::bind::<()>(),
                    sql::bind::<()>(),
                )));

                let ins = sql::Insert::default_tuples()
                    .table(&postings::table)
                    .columns(sql::ident("doing_tag"))
                    .columns(&postings::source)
                    .columns(&postings::entity)
                    .columns(&postings::status)
                    .columns(&postings::doing)
                    .expr(expr)
                    // if a posting already exists for this (doing, source, entity) do nothing
                    .upsert(sql::lit::<()>("ON CONFLICT DO NOTHING"))
                    .as_sql();

                if let Some(log) = Posting::logger() {
                    debug!(
                        log,
                        "RefreshPostings {} ... {:?}",
                        &ins,
                        // rusqlite::params doesn't implement Debug :/
                        (&dt, &source, key, value, &doing)
                    );
                }

                let params = rusqlite::params![dt, source, key, value, doing];
                tx.execute(&ins, params)
            })
            .collect::<Result<Vec<_>, _>>()?;

        Ok(())
    }
}

impl<'l> dispatch::mutable::Also<'l, Interaction<'l>, Note> for RefreshPostings {
    fn after_change(
        &mut self,
        im: &&'l Impetuous,
        tx: &rusqlite::Transaction,
        pk: i64,
        change: dispatch::Change<&Note>,
    ) -> dispatch::Result<()> {
        use impress::orm::Modeled;
        use impress::sql::bind;

        let mut q = DoingTag::q();
        <DoingTag as Modeled>::LinkSet::doing(&mut q).get(|doing| doing.id());
        <DoingTag as Modeled>::LinkSet::tag(&mut q)
            .get(|tag| tag.key().value())
            .filter(|tag| tag.note().eq(bind()));
        q.get(|dt| dt.id()).bind(Box::new(pk));

        RefreshPostings::refresh(im, &tx, q)
    }
}

impl<'l> dispatch::mutable::Also<'l, Interaction<'l>, Doing> for RefreshPostings {
    fn after_change(
        &mut self,
        im: &&'l Impetuous,
        tx: &rusqlite::Transaction,
        pk: i64,
        change: dispatch::Change<&Doing>,
    ) -> dispatch::Result<()> {
        use impress::orm::Modeled;
        use impress::sql::bind;

        let mut q = DoingTag::q();
        <DoingTag as Modeled>::LinkSet::doing(&mut q).get(|doing| doing.id());
        <DoingTag as Modeled>::LinkSet::tag(&mut q).get(|tag| tag.key().value());
        q.get(|dt| dt.id())
            .filter(|dt| dt.doing().eq(bind()))
            .bind(Box::new(pk));

        RefreshPostings::refresh(im, &tx, q)
    }
}

fn tags_in_text<'a>(text: &'a str) -> Vec<Tag> {
    use regex::Regex;

    lazy_static! {
        static ref TAG: Regex = Regex::new(r#"(?:^|\s)[:+]([a-z0-9-]+)(=\S+)?"#).unwrap();
    }

    TAG.captures_iter(text)
        .map(|caps| {
            let tag = caps.get(0).unwrap();
            let key = caps.get(1).unwrap();
            let val = caps
                .get(2)
                // skip the leading =
                .map(|v| v.as_str()[1..].to_owned())
                .unwrap_or(String::new());
            Tag {
                start: maybe(Some(key.start() as u16 - 1)),
                end: maybe(Some(tag.end() as u16)),
                key: maybe(key.as_str().to_string()),
                value: maybe(val),
                ..Tag::default()
            }
        })
        .collect()
}

/// Return a string for a table constraint violation.
/// Should be something like "The <field> ..."
/// (Should this be a complete sentence? (Or several?) Ending in punctuation?)
fn constraint_message(table: &str, constraint: &str) -> Option<String> {
    match (table, constraint) {
        ("doings", "ends_after_start") => {
            "The thing you're doing must end some time after it started."
                .to_string()
                .into()
        }
        (_, "ends_after_start") => "This thing must end after it started.".to_string().into(),
        (_, "text_length") => {
            "The text can't be be empty and can't be more than about 1000 characters long"
                .to_string()
                .into()
        }
        (_, "name_length") => {
            "The name can't be be empty and can't be more than about 500 characters long."
                .to_string()
                .into()
        }
        (_, "key_characters") | (_, "match_key_characters") => {
            "Only lowercase letters, numbers, or a hyphen `-` can be used in keys."
                .to_string()
                .into()
        }
        _ => None,
    }
}

/// __Unknown NOT NULL constraint failed: table.column
/// __Unknown CHECK constraint failed: my_check
/// UniqueViolation UNIQUE constraint failed: foo.bar(, foo.bar)*
fn hint_for_sqlite_constraint_violation(msg: &str) -> Option<String> {
    let parts = msg.splitn(2, " constraint failed: ").collect::<Vec<&str>>();
    match &parts[..] {
        // TODO improve the NOT NULL columns reference?
        ["NOT NULL", columns] => {
            let columns = columns.splitn(2, ".").last().unwrap_or(columns);
            format!("The `{}` cannot be null.", columns).into()
        }
        ["CHECK", check] => match &check.splitn(2, "__").collect::<Vec<&str>>()[..] {
            [table, rule] => constraint_message(table, rule),
            _ => None,
        }
        .or_else(|| Some(format!("This is invalid somehow (hint: {}).", check))),
        ["UNIQUE", constraint] => Some(format!("This is not unique (hint: {}).", constraint)),
        ["start/end overlap"] => "The duration for this thing overlaps another one."
            .to_string()
            .into(),
        _ => None,
    }
}

/// data argument to [Interaction::find] for queries with no params
pub fn unit_deserializer() -> impl serde::Deserializer<'static> {
    use serde::de::IntoDeserializer;
    IntoDeserializer::<serde::de::value::Error>::into_deserializer(())
}

#[cfg(test)]
pub(crate) mod tests {
    use super::*;

    use std::convert::TryInto;

    use crate::{im::MIGRATIONS, migrations::MigrationRequired, serde_ext::FromToml};
    use impress::dispatch::{Changable, Change, Findable, Mutable};
    use impress::orm::Modeled;

    #[test]
    fn test_tags_in_text() -> rusqlite::Result<()> {
        assert_eq!(
            tags_in_text(":fun meeting with @Bob +important=memes"),
            vec![
                Tag {
                    start: maybe(Some(0)),
                    end: maybe(Some(4)),
                    key: maybe("fun".to_string()),
                    value: maybe("".to_string()),
                    ..Tag::default()
                },
                Tag {
                    start: maybe(Some(23)),
                    end: maybe(Some(23 + 16)),
                    key: maybe("important".to_string()),
                    value: maybe("memes".to_string()),
                    ..Tag::default()
                }
            ],
        );

        assert_eq!(
            tags_in_text("send memes to mailto:root?subject=NotATag"),
            vec![],
        );

        assert_eq!(
            tags_in_text("paint +wow :maint=house :with=420-P@1N+erz-Ltd~ :cost=$123"),
            vec![
                Tag::new(6, 6 + 4, "wow", ""),
                Tag::new(11, 11 + 12, "maint", "house"),
                Tag::new(24, 24 + 23, "with", "420-P@1N+erz-Ltd~"),
                Tag::new(48, 48 + 10, "cost", "$123"),
            ],
        );

        Ok(())
    }

    #[test]
    fn test_overlapping_doings() -> rusqlite::Result<()> {
        // // [0-1) [0-2)
        // ({"start": seconds(0), "end": seconds(1)}, {"start": seconds(0), "end": seconds(2)}),
        // ({"start": seconds(0), "end": seconds(2)}, {"start": seconds(0), "end": seconds(1)}),
        // // [1-2) [0-2)
        // ({"start": seconds(1), "end": seconds(2)}, {"start": seconds(0), "end": seconds(2)}),
        // ({"start": seconds(0), "end": seconds(2)}, {"start": seconds(1), "end": seconds(2)}),
        // // [0-3) [1-2)
        // ({"start": seconds(0), "end": seconds(3)}, {"start": seconds(1), "end": seconds(2)}),
        // ({"start": seconds(1), "end": seconds(2)}, {"start": seconds(0), "end": seconds(3)}),

        let conn = test_connection()?;
        Ok(())
    }

    #[test]
    fn test_tags_from_rules() -> anyhow::Result<()> {
        let im = test_impetuous();
        let mut db = test_connection()?;

        let rule_some_value = Rule {
            match_key: maybe("stand"),
            match_value: maybe(Some("up".into())),
            key: maybe("time"),
            value: maybe("well-spent"),
            ..Rule::default()
        };

        let rule_any_value = Rule {
            match_key: maybe("standup"),
            key: maybe("time-well-spent"),
            ..Rule::default()
        };

        super::Interaction::new(&im, &mut db).mutate(&[
            Change::create(&rule_some_value).into(),
            Change::create(&rule_any_value).into(),
            Change::create(&Note {
                text: maybe("attending the daily :standup"),
                ..Note::default()
            })
            .into(),
            Change::create(&Note {
                text: maybe(":stand"),
                ..Note::default()
            })
            .into(),
            Change::create(&Note {
                text: maybe(":stand=up"),
                ..Note::default()
            })
            .into(),
            Change::create(&Note {
                text: maybe(":stand=sideways"),
                ..Note::default()
            })
            .into(),
        ])?;

        {
            let tx = db.transaction()?;
            let tags = Tag::q()
                .get(|tag| tag.key().value())
                .filter(|expr| expr.start().is_null())
                .list(&tx)?;
            assert_eq!(
                tags,
                vec![
                    Tag {
                        key: maybe("time-well-spent"),
                        value: maybe(""),
                        ..Tag::default()
                    },
                    Tag {
                        key: maybe("time"),
                        value: maybe("well-spent"),
                        ..Tag::default()
                    },
                ]
            );
        }

        Ok(())
    }

    #[test]
    fn test_postings_from_tags() -> anyhow::Result<()> {
        let im = test_impetuous();
        let mut db = test_connection()?;

        super::Interaction::new(&im, &mut db).mutate(&[
            Note {
                uuid: maybe("14b08fe3-4273-462c-b49b-fd7dbe701ae9".parse::<Uuid>()?),
                text: maybe("hello world :j=MEME-1"),
                ..Note::default()
            }
            .create()
            .into(),
            Note {
                uuid: maybe("a55419f1-0ef0-4227-8371-d958f9cc3bf2".parse::<Uuid>()?),
                text: maybe("second note :j=MEME-2"),
                ..Note::default()
            }
            .create()
            .into(),
            Rule {
                match_key: maybe("potato"),
                key: maybe("j"),
                value: maybe("MEME-1"),
                ..Rule::default()
            }
            .create()
            .into(),
            Doing {
                uuid: maybe("cd879c23-7dc3-4e70-beab-cb1098996c2c".parse::<Uuid>()?),
                start: maybe("16:00 Feb 20, 2020".parse::<DateTime>()?),
                end: maybe(Some("16:30 Feb 20, 2020".parse::<DateTime>()?)),
                note: maybe(Box::new(Note {
                    uuid: maybe("14b08fe3-4273-462c-b49b-fd7dbe701ae9".parse::<Uuid>()?),
                    ..Note::default()
                })),
                ..Doing::default()
            }
            .create()
            .into(),
            Changable::update(
                &Note {
                    uuid: maybe("14b08fe3-4273-462c-b49b-fd7dbe701ae9".parse::<Uuid>()?),
                    text: maybe("hello world :j=MEME-1"),
                    ..Note::default()
                },
                &Note {
                    uuid: maybe("14b08fe3-4273-462c-b49b-fd7dbe701ae9".parse::<Uuid>()?),
                    text: maybe(":potato :j=MEME-1 :j=MEME-3"),
                    ..Note::default()
                },
            )
            .into(),
        ])?;

        {
            let tx = db.transaction()?;

            let mut q = DoingTag::q();
            <DoingTag as Modeled>::LinkSet::doing(&mut q).get(|doing| doing.id());
            <DoingTag as Modeled>::LinkSet::tag(&mut q).get(|tag| tag.id().key().value());
            eprintln!("doing tags ... {:#?}", q.list(&tx));
        }

        let postings: Box<Vec<Posting>> = super::Interaction::new(&im, &mut db)
            .find("postings".try_into()?, unit_deserializer())?
            .content
            .into_any_box()
            .downcast()
            .expect("TODO add generics to dispatch::Reply and remove this");

        eprintln!("{:#?}", postings);

        Ok(())
    }

    pub fn test_impetuous() -> Impetuous {
        let config = r#"
        [jira]
        api = "jira"
        tag = "j"
        url = "localhost:0"
        "#;
        let config = FromToml::from_toml(config).expect("test config");
        Impetuous {
            db_path: "/dev/null".parse().unwrap(),
            config_path: "/dev/null".parse().unwrap(),
            log: crate::logging::please().clone(),
            config,
            secrets: Default::default(),
            tz: chrono_tz::Tz::UTC,
        }
    }

    pub fn test_connection() -> rusqlite::Result<rusqlite::Connection> {
        let mut db = rusqlite::Connection::open_in_memory()?;

        // todo share this with the non-testing code a bit better so they don't get different
        db.pragma_update(None, "foreign_keys", &true)?;
        crate::sql_funks::add_functions(&mut db)?;

        let tx = db.transaction()?;
        MigrationRequired::from(MIGRATIONS.as_slice())
            .start(&tx)
            .map(|(_, res)| res)
            .collect::<Result<_, _>>()?;
        tx.commit()?;

        Ok(db)
    }
}
