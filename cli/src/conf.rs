use std::collections::HashMap;
use std::{convert::Into, fs, io, path::Path};

use anyhow::Context;
use serde::{Deserialize, Deserializer, Serialize};

use crate::serde_ext::{FromToml, ToToml};
use crate::{server, time::Precision, tracking};

pub static EXAMPLE: &'static str = r#"# This is an example config.toml file for impetuous.

# When doing something, if the thing you're currently doing was started
# fewer than this many seconds ago, rename that current thing instead
# starting a new thing.
# Can be 0 to be disabled.
# rename_sec = 120

# When listing things, show gaps of untracked time if the gaps are longer than
# this number of seconds.
# Can be 0 to be never show gaps.
# show_gap_sec = 1

# When adding times to the database, truncate them to some unit.
# Can be one of ms, sec, or min
# precision = "sec"

# [serve]
# Listen address.  This is the default.
# address = "localhost:7193"
# Extra headers to add to responses.  The default is nothing.
# This is an example of adding a very permissive CORS header.
# headers = { "Access-Control-Allow-Origin" = ["*"] }

# Below is an example of a Jira integration.
# Freshdesk should also work using `api = "freshdesk"`.

# [ext.work]
# api = "jira"
# tag = "j"
# url = "https://work.atlassian.net"

# This will match things you do that contain tags with the key "j";
# like ":j=WORK-1". 
# - The key can contain lowercase letters, numbers, or hyphens.
# - The value can contain anything and is terminated by whitespace.
# Generally, the value is optional but, for posting time, it's used to
# determine what issue/ticket to post to.

# Also, adding a section like this doesn't apply retroactively.  You'll
# only see & try to post time for things you do that are created or edited
# after the section is entered into this your config.

# The section name "work" is persisted in the database.  If you change it,
# impetuous might get confused.  So don't.

# You can also use headers in the Jira/Freshdesk sections like in [serve]
# -- just in case you need to do that for some reason.  But don't add auth
# for that way.

# Add auth for the Jira/Fresdesk services with the secrets.toml file in
# the same directory as this file.  The section name should match the
# section in config.toml but without the "ext." part ...

# In secrets.toml...
# [work]
# basic = "don@fart.photography:hunter2"
"#;

// this implements Serialize in order to write a default file? but maybe EXAMPLE should
// be written instead ...
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(default)]
pub struct Config {
    pub rename_sec: u16,
    pub show_gap_sec: u16,
    pub precision: Precision,
    pub serve: Serve,
    pub colors: Option<bool>,
    #[serde(skip_serializing)]
    #[serde(rename = "ext")]
    #[serde(deserialize_with = "deserialize_ext")]
    pub integrations: HashMap<String, tracking::Integration>,
    #[serde(flatten)]
    pub unknown: HashMap<String, toml::Value>,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            rename_sec: 120,
            show_gap_sec: 1,
            precision: Precision::Second,
            serve: Default::default(),
            colors: None,
            integrations: HashMap::new(),
            unknown: HashMap::new(),
        }
    }
}

pub struct ExampleConfig;

impl ToToml for ExampleConfig {
    fn to_toml(&self) -> Result<String, toml::ser::Error> {
        Ok(EXAMPLE.to_string())
    }
}

impl Into<Config> for ExampleConfig {
    fn into(self) -> Config {
        Config::default()
    }
}

fn deserialize_ext<'de, D>(de: D) -> Result<HashMap<String, tracking::Integration>, D::Error>
where
    D: Deserializer<'de>,
{
    let mut map: HashMap<String, tracking::Integration> = Deserialize::deserialize(de)?;
    for (name, ext) in map.iter_mut() {
        ext.name = name.to_owned();
    }
    Ok(map)
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(default)]
pub struct Serve {
    pub address: std::net::SocketAddr,
    #[serde(with = "crate::serde_ext::fromstr")]
    pub default_content_type: server::ContentType,
    #[serde(with = "crate::hyper_serde")]
    pub headers: hyper::HeaderMap,
}

impl Default for Serve {
    fn default() -> Self {
        Serve {
            address: "127.0.0.1:7193".parse().unwrap(),
            default_content_type: server::ContentType::Json,
            headers: Default::default(),
        }
    }
}

impl Config {
    pub fn rename_duration(&self) -> Option<chrono::Duration> {
        if self.rename_sec == 0 {
            None
        } else {
            Some(chrono::Duration::seconds(self.rename_sec as i64))
        }
    }

    pub fn show_gap_duration(&self) -> Option<chrono::Duration> {
        if self.show_gap_sec == 0 {
            None
        } else {
            Some(chrono::Duration::seconds(self.show_gap_sec as i64))
        }
    }
}

pub type Secrets = HashMap<String, toml::Value>;

pub fn read_or_write_default<P: AsRef<Path>, C>(path: P) -> anyhow::Result<C>
where
    C: FromToml + ToToml + Default,
{
    read_or_write::<_, _, _, C>(path, || Default::default())
}

pub fn read_or_write<P, C, F, T>(path: P, stuff: F) -> anyhow::Result<C>
where
    P: AsRef<Path>,
    F: FnOnce() -> T,
    T: ToToml + Into<C>,
    C: FromToml,
{
    let path = path.as_ref();
    match std::fs::read_to_string(&path) {
        Ok(toml) => C::from_toml(&toml).context("from toml"),
        Err(e) if e.kind() == io::ErrorKind::NotFound => {
            let thing: T = stuff();
            let toml = thing.to_toml().context("to toml")?;
            write_new(path, &toml).context("write")?;
            return Ok(thing.into());
        }
        Err(e) => Err(e)?,
    }
}

pub fn write_new<P: AsRef<Path>>(path: P, contents: &str) -> anyhow::Result<fs::File> {
    use std::io::Write;

    if let Some(parent) = path.as_ref().parent() {
        fs::create_dir_all(parent).with_context(|| format!("mkdir {}", parent.display()))?
    }

    let mut o = fs::OpenOptions::new();
    #[cfg(unix)]
    {
        use std::os::unix::fs::OpenOptionsExt;
        o.mode(0o600);
    }
    let mut file = o.write(true).create_new(true).open(path)?;
    file.write_all(contents.as_bytes())?;
    Ok(file)
}
