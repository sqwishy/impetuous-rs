use structopt::StructOpt;

use impress::maybe;

use crate::{cli::*, domain, Impetuous};

#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
#[structopt(visible_aliases = &["n", "notes"])]
pub struct Note {
    pub blah: Vec<String>,
}

impl Command for Note {
    fn run(self, gen: &General, im: &Impetuous) -> Result<()> {
        if self.blah.is_empty() {
            self.list(gen, im)
        } else {
            self.create(gen, im)
        }
    }
}

impl Note {
    fn list(self, gen: &General, im: &Impetuous) -> Result<()> {
        let mut client = im.client(gen.url.clone())?;

        let q = "notes{text}";
        let notes = client.find::<Vec<domain::Note>, _, serde::de::value::Error>(q, ())?;

        for note in notes.into_iter() {
            println!(
                "- {}",
                note.text.as_ref().map(String::as_str).unwrap_or("...")
            );
        }
        Ok(())
    }

    fn create(self, gen: &General, im: &Impetuous) -> Result<()> {
        let text = self.blah.join(" ");
        let note = domain::Note {
            text: maybe(text),
            ..Default::default()
        };

        let mut client = im.client(gen.url.clone())?;
        client.insert(&note)?;
        Ok(())
    }
}
