use std::ops::Deref;

use anyhow::Result;
use chrono::Duration;
use structopt::StructOpt;

use impress::{dispatch, maybe::maybe};

use crate::{cli::*, domain, Impetuous};

/// Start and stop doing things and recording time.
#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
#[structopt(visible_aliases = &["do", "d"])]
pub struct Doing {
    #[structopt(short, default_value = "now")]
    pub when: DateTimeInput,
    // #[structopt(short, long)]
    // pub append: bool,
    pub blah: Vec<String>,
}

/// It's Thursday morning and you say `im doing -w Wed`, this could mean:
/// - End an entry from Tuesday (or earlier) at this time Wednesday
/// - End an entry from Wednesday/yesterday evening at this time next week's Wednesday
impl Command for Doing {
    fn run(self, gen: &General, im: &Impetuous) -> Result<()> {
        let mut client = im.client(gen.url.clone())?;
        let imprint = im.print();

        let when = self
            .when
            .datetime_at(im.now_local())
            .map(|dt| im.config().precision.trunc(dt))?;
        let text = self.blah.join(" ");

        let current = in_progress(&mut client, when)?;

        // try to rename?
        let should_rename = current.as_ref().and_then(|current| {
            let since = current.started_since(when.into_chrono())?;
            match (since, im.config().rename_duration()) {
                (since, _) if since == Duration::zero() => Some((current, since)),
                (since, Some(rename)) if since < rename => Some((current, since)),
                _ => None,
            }
        });
        if let Some((current, since)) = should_rename {
            if text.is_empty() {
                eprintln!(
                    "I can't stop the thing you're currently doing at {}. It was started {} from then.",
                    imprint.time(when),
                    imprint.chrono_duration(since),
                );
                eprintln!(
                    "If you want to rename it, give this command {}.",
                    imprint.info("some text")
                );
                eprintln!(
                    "Or edit/delete this thing with {}.",
                    imprint.weird("im edit")
                );
                eprintln!(
                    "Or turn this option off by putting {} in your config.toml.",
                    imprint.info("rename_sec = 0"),
                );
                return Ok(()); // TODO technically an error?
            }

            // If we cared, we could check that the current doing note text != text, but then
            // I'd have to figure out what to show the user in that case ...
            let renamed = new_doing_note(&mut client, current.clone(), text)?;

            let unstyled = imprint.clone().unstyled();
            println!(
                "Renamed current thing; started {} from {} ...",
                imprint.chrono_duration(since),
                imprint.time(when),
            );
            println!("{}", imprint.doing(&renamed));
            println!("{}", imprint.old(unstyled.doing(&current).to_string()));

            return Ok(());
        }

        // not renaming, stop the current hing if there is one ...
        let stopped = if let Some(mut current) = current {
            stop_doing(&mut client, &mut current, when)?;
            Some(current)
        } else {
            None
        };

        if !text.is_empty() {
            let doing = domain::Doing {
                start: maybe(when),
                end: maybe(None),
                note: maybe(Box::new(domain::Note {
                    uuid: maybe(domain::Uuid::new_v4()),
                    text: maybe(text),
                    ..Default::default()
                })),
                ..Default::default()
            };

            client.mutate(&[
                dispatch::Change::create(doing.note.as_ref().map(Deref::deref).unwrap()).into(),
                dispatch::Change::create(&doing).into(),
            ])?;

            println!("{}", imprint.doing(&doing));
        } else if stopped.is_none() {
            println!(
                "You were doing nothing before {} {}.",
                imprint.date(when),
                imprint.time(when)
            );
            println!("And you are successfully continuing to do nothing...")
        }

        // Show this at the end, because more recent things should be above ...
        if let Some(stopped) = stopped.as_ref() {
            println!("{}", imprint.doing(stopped));
        }

        Ok(())
    }
}
