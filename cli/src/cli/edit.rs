//! Editing things
use std::borrow::Cow;
use std::collections::HashMap;
use std::iter::IntoIterator;
use std::ops::Deref;

use anyhow::Result;
use structopt::StructOpt;

use impartial::DateishTime;
use impress::dispatch::{self, HasSurrogateKey};
use impress::maybe::{self, Maybe};

use crate::{cli::*, domain, time::DateTime, Impetuous};

/// Edit a list of doings represented as YAML in $EDITOR.
#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
pub struct Edit {
    /// Prompt before applying changes
    #[structopt(short, long)]
    confirm: bool,
    #[structopt(flatten)]
    range: SinceUntil,
    // #[structopt(subcommand)]
    // content: Option<Content>,
}

// #[derive(Debug, StructOpt)]
// enum Content {
//     /// Alter the doings history. (This is the default subcommand)
//     Doing,
//     /// Add or modify information about a Jira instance to post time to
//     Integration,
// }
//
// impl Default for Content {
//     fn default() -> Self {
//         Content::Doing
//     }
// }

impl Command for Edit {
    fn run(self, gen: &General, im: &Impetuous) -> Result<()> {
        let mut client = im.client(gen.url.clone())?;

        let now = im.now_local();
        let since = self.range.since.datetime_on(now.date())?;
        let until = self.range.until.datetime_on(now.date().succ())?;

        let doings: Box<Vec<domain::Doing>> = client.find(
            "doings {uuid, start, end, note {uuid, text}} sort(-start)
                ? and(start.lt.$until, or(end.is.null, end.ge.$since))",
            serde_json::json!({"since": since, "until": until}),
        )?;

        let nice_doings = doings.iter().map(NiceDoing::from).collect::<Vec<_>>();

        let mut editor = InteractiveEdit::builder()
            .header(format!(
                "# Editing entries between {} and {}.",
                since.display(&now.timezone()),
                until.display(&now.timezone()),
            ))
            .footer(format!(
                r#"

# Add, edit, or remove the entries by editing the YAML document above.
#
# Lines starting with # are ignored.
#
# Notes may be shared. If you change note text, it modifies the note for
# anything else with a note with the same uuid. This is good for fixing typos.
# Or you can separate an entry from its note removing the note's `uuid` field
# to create a new note.
#
# Replace the entries with an empty list [] to delete everything.
#
# If you delete an entry, it's gone and so are its records of posted time.
#
# Additions do not have a `uuid` field. Here's a template for you to copy:
# - note:
#     text: "asking jimmie for help"
#   end: ~
#   start: "{}""#,
                im.config()
                    .precision
                    .trunc(DateTime::from(now))
                    .display(&now.timezone()),
            ))
            .write(&nice_doings)
            .context("make temporary file")?;

        loop {
            let res = editor.get_edits().map(|edits: Vec<NiceDoing>| {
                edits
                    .into_iter()
                    .map(Into::into)
                    .collect::<Vec<domain::Doing>>()
            });

            let mut edits = match res {
                Ok(edits) => edits,
                Err(e) => {
                    if editor.should_retry(&e, &im.print()) {
                        continue;
                    } else {
                        return Err(e);
                    }
                }
            };

            {
                // Show the changes to the user...
                //
                // TODO this kind of a lie, but (hopefully) it is mostly true.
                // query/view of the data. But the changes we submit are normalized. Like if a user
                // changes the note, it looks like one doing was changed (the one using the note). But
                // in our request we change just the note and not the doing. Moreover, if the note was
                // shared, it will only appear to be modified for one doing here...
                //
                // Really the notes probably should be denormalized. It's bad/confusing UX. Instead,
                // tags can be used to associate related content (notes, events, doings).
                //
                // But then this issue might come up again if the user is editing posted time entries?
                let imprint = im.print();
                let unstyled = imprint.clone().unstyled();

                let changed = find_deletions(doings.iter(), edits.iter())
                    .chain(find_edits_and_inserts(doings.iter(), edits.iter(), true));
                for dispatch::Change { old, new } in changed {
                    if let Some(old) = old {
                        println!("{}", imprint.old(unstyled.doing(old).to_string()));
                    }
                    if let Some(new) = new {
                        println!("{}", imprint.doing(new));
                    }
                }
            }

            // If notes don't have uuids, assume they're new and set uuids for them
            // so we can reference them from their doings...
            edits.iter_mut().for_each(|d| {
                d.note
                    .as_mut()
                    .map(|note| HasSurrogateKey::default_with(note, uuid::Uuid::new_v4));
            });

            // Copy the sources in case have to retry the edit?
            let mut doings = doings.clone();

            // Here we're peeling the notes off of the doings
            // and replacing them with just their surrogate keys ...
            // appropriate for the mutations request.
            let old_notes = doings
                .iter_mut()
                // .filter_map(|d| d.note.as_ref().map(Box::as_ref))
                .filter_map(|d| replace_with_surrogate(&mut d.note))
                .collect::<Vec<_>>();

            let new_notes = edits
                .iter_mut()
                // .filter_map(|d| d.note.as_ref().map(Box::as_ref))
                .filter_map(|d| replace_with_surrogate(&mut d.note))
                .collect::<Vec<_>>();

            // Find deleted doings first ... (ignore deleted notes ...)
            let mut mutations = find_deletions(doings.iter(), edits.iter())
                .map(|c| c.into())
                // add edits and inserts for notes ...
                .chain(
                    // TODO does this work if a note is shared?
                    find_edits_and_inserts(
                        old_notes.iter().map(Deref::deref),
                        new_notes.iter().map(Deref::deref),
                        false,
                    )
                    .map(|c| c.into()),
                )
                .collect::<Vec<_>>();

            // Then add edits and inserts on doings ...
            let doing_edits =
                find_edits_and_inserts(doings.iter(), edits.iter(), true).map(|c| c.into());
            mutations.extend(doing_edits);

            if mutations.is_empty() {
                eprintln!("Nothing changed!");
                return Ok(());
            }

            if self.confirm
                && !dialoguer::Confirm::new()
                    .with_prompt("Apply changes?")
                    .interact()?
            {
                println!("Cancelling at user request...");
                return Ok(());
            }

            info!(
                im.log(),
                "mutations {}",
                match mutations.len().checked_sub(10) {
                    Some(n) => format!("{:#?} (and {} more)", &mutations[..10], n),
                    None => format!("{:#?}", &mutations),
                }
            );

            if let Err(e) = client.mutate(mutations.as_slice()).context("submit edits") {
                if editor.should_retry(&e, &im.print()) {
                    continue;
                } else {
                    return Err(e);
                }
            }

            break;
        }
        Ok(())
    }
}

/// unique is used to handle when multiple `new`s have the same surrogate key,
/// - if unique, duplicates are considered creations
/// - otherwise, duplicates are treated as updates
///
/// TODO, detect when the new version is not idential to old, but a subset ...?
fn find_edits_and_inserts<'a, M, O, N>(
    old: O,
    new: N,
    unique: bool,
) -> impl Iterator<Item = dispatch::Change<&'a M>>
where
    O: IntoIterator<Item = &'a M>,
    N: IntoIterator<Item = &'a M>,
    M: PartialEq + HasSurrogateKey + 'a,
    <M as HasSurrogateKey>::Type: std::hash::Hash + Eq,
{
    let mut old_by_sk = old
        .into_iter()
        .filter_map(|o| Some((HasSurrogateKey::get(o)?.clone(), o)))
        .collect::<HashMap<_, &M>>();

    new.into_iter().filter_map(move |edit| {
        // for each new item, find a corresponding old one by surrogate key
        let edit_of = if unique {
            HasSurrogateKey::get(edit).and_then(|sk| old_by_sk.remove(sk))
        } else {
            HasSurrogateKey::get(edit)
                .and_then(|sk| old_by_sk.get(sk))
                .cloned()
        };
        if let Some(doing) = edit_of {
            if doing == edit {
                return None;
            }
            Some(dispatch::Change::update(doing, edit))
        } else {
            Some(dispatch::Change::create(edit))
        }
    })
}

fn find_deletions<'a, M, O, N>(old: O, new: N) -> impl Iterator<Item = dispatch::Change<&'a M>>
where
    O: IntoIterator<Item = &'a M>,
    N: IntoIterator<Item = &'a M>,
    M: HasSurrogateKey + 'a,
    <M as HasSurrogateKey>::Type: std::hash::Hash + Eq,
{
    let new_by_sk = new
        .into_iter()
        // filter_map omits edits with no key / insertions
        .filter_map(|d| HasSurrogateKey::get(d).map(|u| (u, d)))
        .collect::<HashMap<_, _>>();

    old.into_iter()
        .filter(move |d| {
            HasSurrogateKey::get(*d)
                .as_ref()
                .map(|sk| !new_by_sk.contains_key(sk))
                .unwrap_or(false)
        })
        .map(|d| dispatch::Change::delete(d))
}

/// If the given link both exists and has a value for its surrogate key, the link is
/// replaced with an instance with _just_ the surrogate key and the original value is
/// returned.
fn replace_with_surrogate<M>(link: &mut Option<Box<M>>) -> Option<Box<M>>
where
    M: Default + HasSurrogateKey + 'static,
{
    let sk = link.as_ref().and_then(HasSurrogateKey::get);
    if let Some(sk) = sk {
        let mut sub = Default::default();
        HasSurrogateKey::set(&mut sub, sk.clone());
        link.replace(Box::new(sub))
    } else {
        None
    }
}

/// I can't figure out if this is really cool or a nightnmare.
#[derive(Serialize, Deserialize)]
struct NiceDoing<'l> {
    #[serde(flatten)]
    doing: Cow<'l, domain::Doing>,

    #[serde(
        default = "maybe::maybe_not",
        deserialize_with = "maybe::deserialize",
        skip_serializing_if = "Option::is_none"
    )]
    end: Maybe<Option<LocalDateishTime>>,
    #[serde(
        default = "maybe::maybe_not",
        deserialize_with = "maybe::deserialize",
        skip_serializing_if = "Option::is_none"
    )]
    start: Maybe<LocalDateishTime>,
}

impl<'l> From<&'l domain::Doing> for NiceDoing<'l> {
    fn from(doing: &'l domain::Doing) -> Self {
        NiceDoing {
            doing: Cow::Borrowed(doing),
            start: doing.start.map(LocalDateishTime).into(),
            end: doing.end.map(|o| o.map(LocalDateishTime)).into(),
        }
    }
}

impl<'l> Into<domain::Doing> for NiceDoing<'l> {
    fn into(self) -> domain::Doing {
        domain::Doing {
            start: self.start.map(Into::into).into(),
            end: self.end.map(|o| o.map(Into::into)).into(),
            ..self.doing.into_owned()
        }
    }
}

/// Uses DateishTime to serialize/deserialize a time, using local today as a hint.
///
/// TODO move to time.rs?
#[derive(Debug, Clone, Copy)]
struct LocalDateishTime(pub DateTime);

impl Into<DateTime> for LocalDateishTime {
    fn into(self) -> DateTime {
        self.0
    }
}

impl serde::ser::Serialize for LocalDateishTime {
    fn serialize<S>(&self, er: S) -> Result<S::Ok, S::Error>
    where
        S: serde::ser::Serializer,
    {
        self.0.display(&chrono::Local).to_string().serialize(er)
    }
}

impl<'de> serde::de::Deserialize<'de> for LocalDateishTime {
    fn deserialize<D>(er: D) -> Result<LocalDateishTime, D::Error>
    where
        D: serde::de::Deserializer<'de>,
    {
        use serde::de::Error;
        let today = chrono::Local::today();
        DateishTime::deserialize(er)
            .and_then(|ish| {
                match ish.datetime_with_date_hint(today) {
                    Some(res) => res.single().ok_or(Error::custom("ambigious time, is this around daylight savings? try adding a UTC offset (like -0700) after the time")),
                    None => Err(Error::custom("missing time information")),
                }
            })
            .map(|dt| dt.with_timezone(&chrono::Utc).into())
            .map(LocalDateishTime)
            .map_err(Error::custom)
    }
}

#[test]
fn test() -> anyhow::Result<()> {
    let doing = domain::Doing {
        start: maybe("16:00 Feb 9 2020".parse::<DateTime>()?),
        end: maybe(Some(
            chrono::Utc
                .ymd(2020, 2, 29)
                // milliseconds are preserved when serialized & deserialized
                .and_hms_milli(17, 30, 0, 123)
                .into(),
        )),
        note: maybe(Box::new(domain::Note {
            text: maybe("derpity derptiy derp"),
            ..domain::Note::default()
        })),
        ..domain::Doing::default()
    };
    let nice = NiceDoing::from(&doing);

    let yaml = serde_yaml::to_value(&nice)?;
    assert_eq!(
        yaml.get("start"),
        Some(&serde_yaml::Value::String(
            "Sun Feb 9 2020 8:00:00 -0800".to_string()
        ))
    );
    assert_eq!(
        yaml.get("end"),
        Some(&serde_yaml::Value::String(
            "Sat Feb 29 2020 9:30:00.123 -0800".to_string()
        ))
    );

    let wow: NiceDoing = serde_yaml::from_value(yaml)?;
    assert_eq!(doing, wow.into());

    Ok(())
}
