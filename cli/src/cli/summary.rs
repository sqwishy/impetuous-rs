use std::collections::BTreeMap;

use anyhow::Result;
use chrono::Duration;
use structopt::StructOpt;

use crate::{cli::*, domain, Impetuous};

#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
#[structopt(visible_aliases = &["ll"])]
pub struct Summary {
    #[structopt(flatten)]
    range: SinceUntil,
}

impl Command for Summary {
    fn run(self, gen: &General, im: &Impetuous) -> Result<()> {
        let mut client = im.client(gen.url.clone())?;

        let config = im.config();
        let imprint = im.print();
        let now = im.now_local();
        let since = self.range.since.datetime_on(now.date())?;
        let until = self.range.until.datetime_on(now.date().succ())?;

        let doings: Box<Vec<domain::Doing>> = client.find(
            "doings {start, end, note {text, tags {key, value}}} sort(-start)
                ? and(start.lt.$until, or(end.is.null, end.ge.$since))",
            serde_json::json!({"since": since, "until": until}),
        )?;

        let mut stuff: BTreeMap<_, BTreeMap<_, Vec<_>>> = Default::default();

        doings
            .iter()
            .filter_map(|doing| {
                let tags = doing.note.as_ref()?.tags.as_ref()?;
                if tags.is_empty() {
                    stuff
                        .entry("")
                        .or_default()
                        .entry("")
                        .or_default()
                        .push(doing)
                }
                let k_v = tags
                    .iter()
                    .filter_map(|tag| Some((tag.key.as_ref()?, tag.value.as_ref()?)));
                for (key, value) in k_v {
                    stuff
                        .entry(key)
                        .or_default()
                        .entry(value)
                        .or_default()
                        .push(doing)
                }
                .into()
            })
            .for_each(|()| {});

        let mut total = std::collections::HashMap::<_, _>::default();

        for (key, value_map) in stuff.iter() {
            let mut key_tally = Tally::new(Duration::zero());

            for (value, doings) in value_map.iter() {
                let mut value_tally = Tally::new(Duration::zero());

                match (*key, *value) {
                    ("", "") => println!("{}", imprint.weird("(untagged)")),
                    (_, "") => println!(":{}", imprint.value(*key)),
                    _ => println!(":{}={}", imprint.key(*key), imprint.value(*value)),
                };

                let mut last_date = None;

                for doing in doings {
                    let duration = doing.duration_or_until(now.into());
                    match duration {
                        Some(duration) if duration > Duration::zero() => {
                            value_tally += duration;

                            doing.start.map(|s| total.entry(s).or_insert(duration));
                        }
                        _ => (),
                    };

                    print!(
                        "  {} {}{}",
                        imprint.start_opt(*doing.start),
                        imprint.duration_opt(duration),
                        imprint.ask(if doing.is_in_progress() == Some(true) {
                            "*"
                        } else {
                            " "
                        }),
                    );

                    match doing.start_date() {
                        Some(date) if Some(date) != last_date => {
                            last_date = Some(date);
                            let fmt = date.format("%A, %B %-d %Y");
                            let date = imprint.style.date.paint(fmt.to_string());
                            print!(" {}", date);
                        }
                        _ => (),
                    };

                    println!("");
                }

                key_tally += *value_tally;

                match (
                    key_tally.into_at_least_two(),
                    value_tally.into_at_least_two(),
                ) {
                    (None, None) => (),
                    (k, v) => {
                        println!(" {} {}", imprint.duration_opt(k), imprint.duration_opt(v),);
                    }
                };
            }

            if let Some(sum) = key_tally.into_at_least_two() {
                println!(" {}", imprint.duration_opt(Some(sum)));
            }
        }

        let total = total
            .values()
            .fold(chrono::Duration::zero(), |sum, d| sum + *d);
        if total > chrono::Duration::zero() {
            println!(
                "{} of time you'll never get back",
                imprint.duration_opt(Some(total))
            )
        }

        Ok(())
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
struct Tally<T>(T, u8);

impl<T> Tally<T> {
    fn new(t: T) -> Self {
        Self(t, 0)
    }

    fn into_at_least_two(self) -> Option<T> {
        if self.has_at_least_two() {
            Some(self.0)
        } else {
            None
        }
    }

    fn has_at_least_two(&self) -> bool {
        self.1 >= 2
    }
}

impl<T> std::ops::Deref for Tally<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T: Default> Default for Tally<T> {
    fn default() -> Self {
        Tally(T::default(), 0)
    }
}

impl<T> std::ops::AddAssign<T> for Tally<T>
where
    T: std::ops::Add<Output = T> + Copy,
{
    fn add_assign(&mut self, other: T) {
        *self = Tally(self.0 + other, self.1 + 1)
    }
}
