use std::path::PathBuf;

use anyhow::{Context, Result};
use chrono::TimeZone;
use rusqlite::types::{FromSql, FromSqlError, FromSqlResult, ValueRef};
use structopt::StructOpt;
use uuid::Uuid;

use crate::{
    cli::{Command, General},
    domain,
    time::DateTime,
    Impetuous,
};
use impress::{dispatch::Changable, maybe, orm::FromKey};

/// Import a database from the Python version of impetuous.
#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
pub struct ImportPy {
    /// Path to old impetuous sqlite database.
    /// It might be at ~/.local/share/impetuous/data.sqlite
    path: PathBuf,
}

struct PyDateTime(DateTime);

impl FromSql for PyDateTime {
    fn column_result(value: ValueRef) -> FromSqlResult<Self> {
        value.as_i64().and_then(|seconds| {
            chrono::Utc
                .timestamp_opt(seconds, 0)
                .single()
                .ok_or_else(|| FromSqlError::OutOfRange(seconds))
                .map(DateTime::from)
                .map(PyDateTime)
        })
    }
}

impl Command for ImportPy {
    fn run(self, gen: &General, im: &Impetuous) -> Result<()> {
        let mut client = im.client(gen.url.clone())?;

        let path = self.path.canonicalize().unwrap();
        let db = rusqlite::Connection::open_with_flags(
            &self.path,
            rusqlite::OpenFlags::SQLITE_OPEN_READ_ONLY,
        )
        .with_context(|| format!("open {}", self.path.display()))?;

        // todo there is a comment column that I forgot about and ignore?
        let mut stmt = db
            .prepare(r#"select id, text, start, "end" from entry"#)
            .context("prepare query")?;

        let note_doings = stmt
            .query_map(rusqlite::NO_PARAMS, |row| {
                Ok((
                    row.get::<_, Uuid>(0)?,
                    row.get::<_, String>(1)?,
                    row.get::<_, PyDateTime>(2)?.0,
                    row.get::<_, Option<PyDateTime>>(3)?.map(|py| py.0),
                ))
            })
            .context("query rows")?
            .map(|row| -> rusqlite::Result<_> {
                let (uuid, text, start, end) = row?;
                Ok(from_pydoing(uuid, text, start, end))
            })
            .collect::<Result<Vec<_>, _>>()?;

        let mut stmt = db
            .prepare(r#"select id, entry, ext, "key", result from submission"#)
            .context("prepare query")?;

        let postings = stmt
            .query_map(rusqlite::NO_PARAMS, |row| {
                Ok((
                    row.get::<_, Uuid>(0)?,
                    row.get::<_, Uuid>(1)?,
                    row.get::<_, String>(2)?,
                    row.get::<_, String>(3)?,
                    row.get::<_, String>(4)?,
                ))
            })
            .context("query rows")?
            .map(|row| -> rusqlite::Result<_> {
                let (uuid, doing, source, entity, detail) = row?;
                Ok(from_pyposting(uuid, doing, source, entity, detail))
            })
            .collect::<Result<Vec<_>, _>>()?;

        let changes = note_doings
            .iter()
            .map(|(note, doing)| {
                std::iter::once(note.create().into()).chain(std::iter::once(doing.create().into()))
            })
            .flatten()
            .chain(postings.iter().map(|p| p.create().into()))
            .collect::<Vec<_>>();

        info!(im.log(), "{:#?}", changes);

        let imprint = im.print();
        println!(
            "Importing {} doings & notes and {} posted time entries ...",
            imprint.info(note_doings.len().to_string()),
            imprint.info(changes.len().to_string()),
        );

        client.mutate(changes.as_slice())?;

        let imprint = im.print();
        println!("{}", imprint.good("Success!"));

        Ok(())
    }
}

fn from_pydoing(
    uuid: Uuid,
    text: String,
    start: DateTime,
    end: Option<DateTime>,
) -> (domain::Note, domain::Doing) {
    let note_uuid = Uuid::new_v4();
    let note = domain::Note {
        uuid: maybe(note_uuid),
        text: maybe(text),
        ..domain::Note::default()
    };
    let doing = domain::Doing {
        uuid: maybe(uuid),
        end: maybe(end),
        start: maybe(start),
        note: maybe::<_, Box<_>>(FromKey::from_key(note_uuid)),
        ..domain::Doing::default()
    };
    (note, doing)
}

fn from_pyposting(
    uuid: Uuid,
    doing: Uuid,
    source: String,
    entity: String,
    detail: String,
) -> domain::Posting {
    domain::Posting {
        uuid: maybe(uuid),
        doing: maybe::<_, Box<_>>(FromKey::from_key(doing)),
        source: maybe(source),
        entity: maybe(entity),
        status: maybe(domain::PostingStatus::Success),
        detail: maybe(detail),
        ..domain::Posting::default()
    }
}
