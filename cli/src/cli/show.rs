use anyhow::Result;
use structopt::StructOpt;

use crate::{cli::*, domain, Impetuous};

#[derive(Debug, StructOpt)]
#[structopt(setting(clap::AppSettings::ColoredHelp))]
#[structopt(visible_aliases = &["l", "s"])]
pub struct Show {
    #[structopt(flatten)]
    range: SinceUntil,
    #[structopt(short, long)]
    limit: Option<i64>,
}

impl Command for Show {
    fn run(self, gen: &General, im: &Impetuous) -> Result<()> {
        let mut client = im.client(gen.url.clone())?;

        let config = im.config();
        let now = im.now_local();
        let since = self.range.since.datetime_on(now.date())?;
        let until = self.range.until.datetime_on(now.date().succ())?;

        let doings: Box<Vec<domain::Doing>> = client.find(
            "doings {start, end, note {text}, postings{entity,status}} sort(-start)
                ? and(start.lt.$until, or(end.is.null, end.ge.$since))
                ..$limit",
            serde_json::json!({"since": since, "until": until, "limit": self.limit.unwrap_or(-1)}),
        )?;

        if doings.is_empty() {
            println!("You've done nothing!");
        }

        im.print().print_doings(doings.as_slice());

        Ok(())
    }
}
