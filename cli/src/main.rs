#![cfg_attr(backtrace, feature(backtrace))]
#![allow(dead_code)]
#![allow(unused_variables)]
// ^^^ what the shit; get a hold of yourself

#[macro_use]
extern crate serde;

#[macro_use]
extern crate slog;

pub use imql;

mod cli;
mod client;
mod conf;
mod domain;
mod hyper_serde;
mod im;
mod logging;
mod migrations;
// mod result_ext;
mod serde_ext;
mod server;
mod sql_funks;
mod time;
mod tracking;
mod web;

pub use conf::Config;
pub use im::Impetuous;
pub use time::DateTime;

fn main() -> anyhow::Result<()> {
    // TODO maybe display errors nicer?
    // Although the default error handling is not terrible ...
    cli::main()
}

#[cfg(test)]
pub(crate) fn run_in_tokio<F>(fut: F) -> F::Output
where
    F: std::future::Future + Send,
    F::Output: Send,
{
    use tokio::{runtime, task};
    let mut rt = runtime::Builder::new()
        .enable_io()
        .enable_time()
        .basic_scheduler()
        .build()
        .unwrap();
    let local = task::LocalSet::new();
    local.block_on(&mut rt, fut)
}
