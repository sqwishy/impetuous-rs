//! time tracking to Jira
use std::{
    collections::HashMap,
    convert::{TryFrom, TryInto},
    fmt,
    future::Future,
    pin::Pin,
};

use anyhow::{Context, Error, Result};
use bytes::buf::Buf;
use futures::FutureExt;
use http::response;
use hyper::{
    body::Bytes,
    client::HttpConnector,
    header::{HeaderValue, ACCEPT, AUTHORIZATION, CONTENT_TYPE},
    Body, Request,
};
use hyper_tls::HttpsConnector;

use crate::serde_ext::FromToml;
use crate::{
    domain,
    web::{self, JoinWith},
};

/// A specific use of a Ledger, since someone might post to multiple Jiras or something
#[derive(Clone, Debug, Deserialize)]
pub struct Integration {
    // this is populated by the Config deserializer
    #[serde(skip)]
    pub name: String,
    pub tag: String,
    #[serde(flatten)]
    pub ledger: LedgerOptions,
}

impl Integration {
    pub fn unknown(&self) -> &HashMap<String, toml::Value> {
        match &self.ledger {
            LedgerOptions::Jira(o) => &o.unknown,
            LedgerOptions::Derpdesk(o) => &o.unknown,
        }
    }

    pub fn open_ledger(&self, secrets: toml::Value) -> Result<Box<dyn LedgerErased + Send + Sync>> {
        match &self.ledger {
            LedgerOptions::Jira(o) => {
                let secrets = <Jira as Ledger>::Secrets::from_value(secrets)?;
                let ledger = Jira::new(o.clone(), secrets)?;
                Ok(Box::new(ledger))
            }
            LedgerOptions::Derpdesk(o) => {
                let secrets = <Derpdesk as Ledger>::Secrets::from_value(secrets)?;
                let ledger = Derpdesk::new(o.clone(), secrets)?;
                Ok(Box::new(ledger))
            }
        }
    }

    // pub fn source(&self) -> String {
    //     match &self.ledger {
    //         LedgerOptions::Jira(o) => o.url.to_string(),
    //     }
    // }
}

/// Which Ledger to use ...
#[derive(Clone, Debug, Deserialize)]
#[serde(tag = "api")]
pub enum LedgerOptions {
    #[serde(rename = "jira")]
    Jira(<jira::Jira as Ledger>::Options),
    #[serde(rename = "freshdesk")]
    Derpdesk(<derpdesk::Derpdesk as Ledger>::Options),
}

#[derive(Clone, Debug, Deserialize)]
pub struct HttpOptions {
    #[serde(with = "crate::serde_ext::fromstr")]
    pub url: url::Url,
    #[serde(default)]
    #[serde(with = "crate::hyper_serde")]
    pub headers: hyper::HeaderMap,
    /// unrecognized options, captured as to warn the user
    #[serde(flatten)]
    pub unknown: HashMap<String, toml::Value>,
}

/// You might be able to use https://id.atlassian.com/manage/api-tokens
/// instead of your password.
#[derive(Debug, Deserialize)]
pub struct HttpSecrets {
    pub basic: String,
}

#[derive(Debug)]
pub struct StatusUpdate {
    status: domain::PostingStatus,
    detail: String,
}

type SubmitItem = Result<String>;

pub trait Ledger: fmt::Debug {
    type Secrets: FromToml;
    type Options: FromToml;

    fn new(_: Self::Options, _: Self::Secrets) -> Result<Self>
    where
        Self: Sized;

    fn address(&self) -> &str;

    /// how would coalescing multiple doings into a single submission work?
    fn submit(
        &'_ self,
        _: domain::Posting,
    ) -> Pin<Box<dyn Future<Output = SubmitItem> + Send + '_>>;
}

/// The ledger cannot be boxed nicely because of its trait items.
/// This is a type-erased trait for it.
pub trait LedgerErased: fmt::Debug {
    fn address_erased(&self) -> &str;

    fn submit_erased(
        &'_ self,
        _: domain::Posting,
    ) -> Pin<Box<dyn Future<Output = SubmitItem> + Send + '_>>;
}

impl<L> LedgerErased for L
where
    L: Ledger,
{
    fn address_erased(&self) -> &str {
        self.address()
    }

    fn submit_erased(
        &'_ self,
        posting: domain::Posting,
    ) -> Pin<Box<dyn Future<Output = SubmitItem> + Send + '_>> {
        self.submit(posting)
    }
}

fn https_client() -> hyper::Client<HttpsConnector<HttpConnector>, hyper::Body> {
    let https = HttpsConnector::new();
    hyper::Client::builder().build::<_, hyper::Body>(https)
}

pub use jira::Jira;

pub mod jira {
    use super::*;

    #[derive(Debug)]
    pub struct Jira {
        secrets: HttpSecrets,
        options: HttpOptions,
    }

    impl Ledger for Jira {
        type Secrets = HttpSecrets;
        type Options = HttpOptions;

        fn new(options: Self::Options, secrets: Self::Secrets) -> Result<Self>
        where
            Self: Sized,
        {
            Ok(Jira { secrets, options })
        }

        fn address(&self) -> &str {
            self.options.url.as_str()
        }

        fn submit(
            &'_ self,
            posting: domain::Posting,
        ) -> Pin<Box<dyn Future<Output = SubmitItem> + Send + '_>> {
            let fut = async move {
                let issue = posting
                    .entity
                    .as_ref()
                    .ok_or(anyhow::Error::msg("missing posting source"))?;

                let worklog: Worklog = posting
                    .doing
                    .as_ref()
                    .ok_or(anyhow::Error::msg("missing doing"))
                    .and_then(|doing| doing.as_ref().try_into())?;

                let path = format!(
                    "/rest/api/2/issue/{}/worklog?notifyUsers=false",
                    web::encode_path(&issue).to_string(),
                );

                let http = HttpIntegration::new(&self.options, &self.secrets);
                http.post(&path, &worklog)
                    .map(|res| res.map(HttpIntegration::response_details))
                    .await?
            };
            Box::pin(fut)
        }
    }

    /// The document posted to the Jira API
    #[derive(Debug, Serialize)]
    pub struct Worklog {
        pub started: String,
        /// must be a multiple of 60
        #[serde(rename = "timeSpentSeconds")]
        pub time_spent_seconds: i64,
    }

    impl TryFrom<&domain::Doing> for Worklog {
        type Error = anyhow::Error;
        fn try_from(doing: &domain::Doing) -> anyhow::Result<Worklog> {
            let started = doing
                .start
                .ok_or(Error::msg("missing start time"))?
                .format("%Y-%m-%dT%H:%M:%S%.3f%z")
                .to_string();
            let time_spent_seconds = doing
                .duration()
                .ok_or(Error::msg("missing end time"))?
                .num_seconds();
            Ok(Worklog {
                started,
                time_spent_seconds,
            })
        }
    }
}

pub use derpdesk::Derpdesk;

pub mod derpdesk {
    use super::*;

    #[derive(Debug)]
    pub struct Derpdesk {
        secrets: HttpSecrets,
        options: HttpOptions,
    }

    impl Ledger for Derpdesk {
        type Secrets = HttpSecrets;
        type Options = HttpOptions;

        fn new(options: Self::Options, secrets: Self::Secrets) -> Result<Self>
        where
            Self: Sized,
        {
            Ok(Derpdesk { secrets, options })
        }

        fn address(&self) -> &str {
            self.options.url.as_str()
        }

        fn submit(
            &'_ self,
            posting: domain::Posting,
        ) -> Pin<Box<dyn Future<Output = SubmitItem> + Send + '_>> {
            let fut = async move {
                let ticket = posting
                    .entity
                    .as_ref()
                    .ok_or(anyhow::Error::msg("missing posting source"))?;

                let timeentry: TimeEntry = posting
                    .doing
                    .as_ref()
                    .ok_or(anyhow::Error::msg("missing doing"))
                    .and_then(|doing| doing.as_ref().try_into())?;

                let path = format!(
                    "/api/v2/tickets/{}/time_entries",
                    web::encode_path(&ticket).to_string(),
                );

                let http = HttpIntegration::new(&self.options, &self.secrets);
                http.post(&path, &timeentry)
                    .map(|res| res.map(HttpIntegration::response_details))
                    .await?
            };
            Box::pin(fut)
        }
    }

    /// The document posted to the Derpdesk API
    #[derive(Debug, Serialize)]
    pub struct TimeEntry {
        /// h:m
        pub time_spent: String,
        /// iso 8601 format?
        pub executed_at: String,
        pub note: &'static str,
        // TODO use a tag for this
        pub billable: bool,
        pub timer_running: bool,
    }

    impl TryFrom<&domain::Doing> for TimeEntry {
        type Error = anyhow::Error;
        fn try_from(doing: &domain::Doing) -> anyhow::Result<TimeEntry> {
            let executed_at = doing
                .start
                .ok_or(Error::msg("missing start time"))?
                .format("%Y-%m-%dT%H:%M:%S%.3f%z")
                .to_string();
            let minutes_spent = doing
                .duration_minutes()
                .ok_or(Error::msg("missing end time"))?;
            let time_spent = format!("{}:{:02}", minutes_spent / 60, minutes_spent % 60);
            Ok(TimeEntry {
                time_spent,
                executed_at,
                note: "",
                billable: false,
                timer_running: false,
            })
        }
    }
}

pub struct HttpIntegration<'l> {
    options: &'l HttpOptions,
    secrets: &'l HttpSecrets,
    client: hyper::Client<HttpsConnector<HttpConnector>, hyper::Body>,
}

impl<'l> HttpIntegration<'l> {
    pub fn new(options: &'l HttpOptions, secrets: &'l HttpSecrets) -> Self {
        HttpIntegration {
            options,
            secrets,
            client: https_client(),
        }
    }

    /// posts json to the given path joined with the options url
    pub async fn post<P, S>(&self, path: P, data: &S) -> Result<(response::Parts, Bytes)>
    where
        P: AsRef<str>,
        S: serde::Serialize,
    {
        let req = self.new_json_post_request(path, data)?;
        let (head, body) = self.client.request(req).await?.into_parts();
        let body = hyper::body::aggregate(body).await?.to_bytes();
        // TODO logging the response is helpful here but could leak cookies or other secret shit
        Ok((head, body))
    }

    pub fn new_json_post_request<P, S>(&self, path: P, data: &S) -> Result<Request<Body>>
    where
        P: AsRef<str>,
        S: serde::Serialize,
    {
        let json = serde_json::to_string(&data)?;

        let base = &self.options.url;
        let url: hyper::Uri = base
            .join_with("/", &path.as_ref())
            .parse()
            .with_context(|| format!("could not url: {}{}", base, path.as_ref()))?;

        let mut req = Request::post(url).body(json.into())?;

        req.headers_mut()
            .insert(ACCEPT, HeaderValue::from_static("application/json"));
        req.headers_mut()
            .insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
        req.headers_mut().extend(self.options.headers.clone());

        let log = crate::logging::please();
        debug!(log, "request: {:#?}", req);

        let auth = format!("Basic {}", base64::encode(&self.secrets.basic));
        let auth = HeaderValue::from_str(&auth)?;
        req.headers_mut().insert(AUTHORIZATION, auth);

        Ok(req)
    }

    /// converts a head-body response pair into a Result<String>/posting status & detail
    pub fn response_details(resp: (response::Parts, Bytes)) -> SubmitItem {
        let (head, body) = resp;
        let body = String::from_utf8(body.to_vec()).unwrap_or_else(|_| format!("{:?}", body));
        if !head.status.is_success() {
            return Err(Error::msg(format!("{}: {}\n{:?}", head.status, body, head)));
        }
        Ok(format!("{}\n{:?}", body, head))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::{net::TcpListener, sync::Arc};

    use http::request;
    use hyper::service::{make_service_fn, service_fn};
    use hyper::{body::Buf, Body, Response};
    use parking_lot::Mutex;

    use crate::domain::{Doing, Posting};
    use crate::time::DateTime;
    use impress::maybe;

    #[test]
    fn jira_submit() -> Result<()> {
        let sock = TcpListener::bind("localhost:0")?;
        let port = sock.local_addr()?.port();

        let posting = Posting {
            entity: maybe("MEME-1"),
            doing: maybe(Box::new(Doing::new(
                "16:00 Feb 20, 2020".parse::<DateTime>()?,
                Some("16:30 Feb 20, 2020".parse::<DateTime>()?),
                "hello world :j=MEME-1",
            ))),
            ..Posting::default()
        };

        let options = <Jira as Ledger>::Options::from_toml(&format!(
            r#"url = "http://localhost:{}""#,
            port,
        ))?;
        let secrets = <Jira as Ledger>::Secrets::from_toml(r#"basic = "derp:hunter2""#)?;

        let j = Jira::new(options, secrets)?;
        let fut = j.submit(posting);

        let good_job = hyper::Response::new(hyper::Body::from("some happy message"));
        let (_, submit_body) = serve_canned_reply(sock, good_job, fut);
        // this just checks that a request was made, not the result of submit()
        assert_eq!(
            serde_json::from_slice::<serde_json::Value>(submit_body.bytes())?,
            serde_json::json! ({
                "started": "2020-02-20T16:00:00.000+0000",
                "timeSpentSeconds": 1800,
            }),
        );

        Ok(())
    }

    /// Run the `send_request future, wait for a request on `sock`, and reply with `response`.
    /// Return the request that was seen by the server.
    fn serve_canned_reply<F, O>(
        sock: TcpListener,
        response: Response<Body>,
        submit: F,
    ) -> (request::Parts, impl hyper::body::Buf)
    where
        F: Future<Output = O> + Unpin + Send,
        O: std::fmt::Debug,
    {
        let canned = Arc::new(Mutex::new(Some(response)));
        let capture = Arc::new(Mutex::new(None));
        let capture_ = capture.clone();

        crate::run_in_tokio(async move {
            use std::convert::Infallible;

            let make_service = make_service_fn(|_conn| {
                let canned = canned.clone();
                let capture = capture.clone();

                async move {
                    Ok::<_, Infallible>(service_fn(move |req| {
                        let capture = capture.clone();
                        let resp = canned.lock().take().expect("out of canned responses");
                        let (head, body) = req.into_parts();
                        async move {
                            let body = hyper::body::aggregate(body).await?;
                            capture.lock().replace((head, body));
                            Ok::<_, hyper::Error>(resp)
                        }
                    }))
                }
            });

            let server = hyper::Server::from_tcp(sock).expect("hyper::Server::from_tcp");
            let server = server.serve(make_service);

            let mut server = server.fuse();
            let mut submit = submit.fuse();

            let res = futures::select! {
                res = server => {
                    panic!("server quit {:#?}", res);
                },
                res = submit => res,
            };
            eprintln!("submit result: {:?}", res);
        });

        let mut l = capture_.lock();
        l.take().expect("did not capture request?")
    }
}
