use std::sync::atomic::{AtomicU8, Ordering};
use std::sync::Arc;

use slog::Drain;
pub use slog::Level;

lazy_static::lazy_static! {
    pub static ref ROOT_LOGGER: (slog::Logger, Arc<AtomicU8>) = {
        use slog::Drain;

        let decorator = slog_term::TermDecorator::new().build();
        let drain = slog_term::CompactFormat::new(decorator).build();
        let drain = LevelFilter::new(drain, Level::Debug);
        let filter = drain.at_least.clone();
        let drain = drain.fuse();
        let drain = std::sync::Mutex::new(drain).fuse();
        let log = slog::Logger::root(drain, o!());
        (log, filter)
    };
}

pub fn set_level(l: Level) {
    ROOT_LOGGER.1.store(level_to_u8(l), Ordering::Relaxed)
}

/// slog::Level implements FromStr with Err = () but StructOpt wants a stringy error.
pub fn parse_level(s: &str) -> anyhow::Result<Level> {
    s.parse()
        .map_err(|()| anyhow::Error::msg("invalid log level"))
}

struct LevelFilter<D> {
    drain: D,
    at_least: Arc<AtomicU8>,
}

fn level_to_u8(l: Level) -> u8 {
    use slog::Level::*;
    match l {
        Critical => 5,
        Error => 4,
        Warning => 3,
        Info => 2,
        Debug => 1,
        Trace => 0,
    }
}

fn u8_to_level(l: u8) -> Level {
    use slog::Level::*;
    match l {
        5 => Critical,
        4 => Error,
        3 => Warning,
        2 => Info,
        1 => Debug,
        0 => Trace,
        _ => Debug,
    }
}

impl<D> LevelFilter<D> {
    fn new(drain: D, level: Level) -> Self {
        let at_least = Arc::new(AtomicU8::new(level_to_u8(level)));
        LevelFilter { drain, at_least }
    }
}

impl<D> Drain for LevelFilter<D>
where
    D: Drain,
{
    type Ok = Option<D::Ok>;
    type Err = Option<D::Err>;

    fn log(
        &self,
        record: &slog::Record,
        values: &slog::OwnedKVList,
    ) -> Result<Self::Ok, Self::Err> {
        let at_least = u8_to_level(self.at_least.load(Ordering::Relaxed));

        if record.level().is_at_least(at_least) {
            self.drain.log(record, values).map(Some).map_err(Some)
        } else {
            Ok(None)
        }
    }
}

pub fn please() -> &'static slog::Logger {
    &ROOT_LOGGER.0
}
