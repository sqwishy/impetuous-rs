use std::fmt;
use std::ops::{Deref, DerefMut};
use std::str::FromStr;

use chrono::{TimeZone, Utc};

use rusqlite::types::{FromSql, FromSqlError, FromSqlResult, ToSql, ToSqlOutput, ValueRef};

/// Impetuous models datetimes with millisecond resolution.
///
/// Instances of this type _should_ be truncated by the ctor [new_lossy].
///
/// SQLite can store this nicely in a 64bit integer.
#[derive(Debug, Copy, Clone, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(from = "chrono::DateTime<Utc>")] /* TODO XXX FIXME the API can only deserialize UTC datetimes ........ */
pub struct DateTime(chrono::DateTime<Utc>);

impl DateTime {
    /// Makes an `DateTime` from a [DateTime<Utc>](chrono::DateTime),
    /// truncates things smaller than a millisecond.
    ///
    /// (Might also fuck up leap seconds ... who knows?)
    pub fn new_lossy(dt: chrono::DateTime<Utc>) -> Self {
        use chrono::SubsecRound;

        DateTime(dt.trunc_subsecs(3))
    }

    pub fn now() -> Self {
        Utc::now().into()
    }

    pub fn into_chrono(self) -> chrono::DateTime<Utc> {
        self.0
    }

    pub fn as_chrono(&self) -> &chrono::DateTime<Utc> {
        &self.0
    }

    pub fn map<F>(self, func: F) -> Self
    where
        F: Fn(chrono::DateTime<Utc>) -> chrono::DateTime<Utc>,
    {
        Self(func(self.0))
    }

    pub fn display<Tz>(&self, tz: &Tz) -> impl fmt::Display
    where
        Tz: TimeZone,
        <Tz as TimeZone>::Offset: fmt::Display,
    {
        let fmt = if self.timestamp_subsec_millis() == 0 {
            "%a %b %-d %Y %-H:%M:%S %z"
        } else {
            "%a %b %-d %Y %-H:%M:%S%.3f %z"
        };
        self.with_timezone(tz).format(fmt)
    }
}

/// Parses a string that might resemble a date/time in UTC.
impl FromStr for DateTime {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse::<impartial::DateishTime>()
            .and_then(|ish| {
                ish.datetime_utc()
                    .ok_or(anyhow::Error::msg("missing time information"))
            })
            .map(DateTime::new_lossy)
    }
}

impl<Tz: TimeZone> From<chrono::DateTime<Tz>> for DateTime {
    fn from(other: chrono::DateTime<Tz>) -> Self {
        DateTime::new_lossy(other.with_timezone(&Utc))
    }
}

impl Deref for DateTime {
    type Target = chrono::DateTime<Utc>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for DateTime {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl ToSql for DateTime {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput> {
        Ok(self.timestamp_millis().into())
    }
}

impl FromSql for DateTime {
    fn column_result(value: ValueRef) -> FromSqlResult<Self> {
        value.as_i64().and_then(|v| {
            Utc.timestamp_millis_opt(v)
                .single()
                // I don't think this can happen with UTC?
                .ok_or_else(|| FromSqlError::OutOfRange(v))
                .map(chrono::DateTime::<Utc>::from)
                .map(DateTime)
        })
    }
}

#[derive(Clone, Copy, PartialEq, Debug, Serialize, Deserialize)]
pub enum Precision {
    #[serde(rename = "ms")]
    #[serde(alias = "milli")]
    #[serde(alias = "millis")]
    #[serde(alias = "millisecond")]
    #[serde(alias = "milliseconds")]
    Millisecond,
    #[serde(rename = "sec")]
    #[serde(alias = "s")]
    #[serde(alias = "secs")]
    #[serde(alias = "second")]
    #[serde(alias = "seconds")]
    Second,
    #[serde(rename = "min")]
    #[serde(alias = "m")]
    #[serde(alias = "mins")]
    #[serde(alias = "minute")]
    #[serde(alias = "minutes")]
    Minute,
}

impl Precision {
    /// floor/truncate
    pub fn trunc(self, dt: DateTime) -> DateTime {
        use chrono::{NaiveTime, SubsecRound, Timelike};
        let date = dt.date();
        let time = dt.time();
        let time = match self {
            Precision::Millisecond => time.trunc_subsecs(3),
            Precision::Second => time.trunc_subsecs(0),
            Precision::Minute => NaiveTime::from_hms(time.hour(), time.minute(), 0),
        };
        date.and_time(time).unwrap().into()
    }
}

/// Read a chrono_tz time zone from TZ or /etc/localtime
pub fn current_tz() -> anyhow::Result<chrono_tz::Tz> {
    use anyhow::Context;

    if let Ok(tz) = std::env::var("TZ") {
        if let Ok(tz) = tz.parse() {
            return Ok(tz);
        }
        eprintln!("warning: ignoring TZ since `{}` is not a timezone", tz);
    }

    let path = std::fs::read_link("/etc/localtime").context("read /etc/localtime")?;
    let mut components = path.components();
    let b = components.next_back().and_then(|c| c.as_os_str().to_str());
    let a = components.next_back().and_then(|c| c.as_os_str().to_str());
    match (a, b) {
        (Some(a), Some(b)) => format!("{}/{}", a, b).parse(),
        _ => Err(format!("no time zone in: {}", path.display())),
    }
    .map_err(|msg| anyhow::Error::msg(msg))
}

#[cfg(test)]
mod tests {
    use super::{DateTime, Utc};

    use chrono::TimeZone;

    #[test]
    fn test_lossy_time() {
        // this is also a doctest but cargo/shit is broken and dumb...
        // https://github.com/rust-lang/rust/issues/50784

        let dt = Utc.ymd(1970, 1, 1).and_hms_milli(0, 0, 1, 444);
        assert_eq!(DateTime::new_lossy(dt), DateTime(dt));

        let dt_with_nanos = Utc.ymd(1970, 1, 1).and_hms_micro(0, 0, 1, 444_555);
        assert_eq!(DateTime::new_lossy(dt_with_nanos), DateTime(dt));
    }

    #[test]
    fn precision() {
        use super::Precision::*;
        let date = Utc.ymd(1970, 1, 1);
        assert_eq!(
            Millisecond.trunc(date.and_hms_micro(1, 2, 3, 444_555).into()),
            date.and_hms_milli(1, 2, 3, 444).into(),
            "truncate ms"
        );
        assert_eq!(
            Second.trunc(date.and_hms_milli(1, 2, 3, 444).into()),
            DateTime::from(date.and_hms(1, 2, 3)),
            "truncate second",
        );
        assert_eq!(
            Minute.trunc(date.and_hms_milli(1, 2, 3, 444).into()),
            DateTime::from(date.and_hms(1, 2, 0)),
            "truncate minute"
        );
        assert_eq!(
            Minute.trunc(date.and_hms_milli(1, 2, 59, 444).into()),
            DateTime::from(date.and_hms(1, 2, 0)),
            "truncate minute"
        );
    }
}
