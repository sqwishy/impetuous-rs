use std::convert::TryInto;

use hyper::{
    client::HttpConnector,
    header::{HeaderValue, ACCEPT, CONTENT_TYPE},
};
use hyper_tls::HttpsConnector;
use serde::{de::IntoDeserializer, Deserialize, Serialize};

use impress::dispatch::{self, BorrowChange, Change, Findable, Mutable};

use crate::{
    domain,
    im::Impetuous,
    web::{encode_path, JoinWith},
};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error(transparent)]
    Imql(#[from] imql::Error),
    #[error(transparent)]
    Dispatch(#[from] dispatch::Error),
    #[error("unexpected in-process response object")]
    InvalidContainer,
    #[error("failed to serialize request: {0}")]
    Ser(#[from] serde_cbor::Error),
    #[error("failed making http request: {0}")]
    Hyper(#[from] hyper::Error),
    #[error("failed making http request: {0}")]
    Http(#[from] http::Error),
}

pub type Result<T, E = Error> = std::result::Result<T, E>;

// pub type Address = Option<hyper::http::Uri>;
//
// pub fn in_process_address() -> Address {
//     None
// }
//
// pub fn http_address(uri: hyper::http::Uri) -> Address {
//     Some(uri)
// }

pub enum Via {
    InProcess {
        db: rusqlite::Connection,
    },
    Http {
        client: hyper::Client<HttpsConnector<HttpConnector>, hyper::Body>,
        url: hyper::Uri,
        // headers ...
    },
}

impl Via {
    pub fn http(url: hyper::Uri) -> Via {
        let client = hyper::Client::builder()
            .http2_only(true)
            .build::<_, hyper::Body>(HttpsConnector::new());
        Via::Http { url, client }
    }
}

/// TODO the methods in here probably shouldn't require a mutable reference.
/// Either we should use a lock or support some concurrency and make that opaque to users.
pub struct Client<'a> {
    pub log: slog::Logger,
    pub via: Via,
    pub im: &'a Impetuous,
}

impl<'a> Client<'a> {
    fn _find_via_db<D, E>(&mut self, query: &str, data: D) -> Result<dispatch::Reply>
    where
        for<'de> D: IntoDeserializer<'de, E>,
        E: serde::de::Error,
    {
        match &mut self.via {
            Via::InProcess { ref mut db } => {
                let query: imql::Query<&str> = query.try_into()?;

                let rep = domain::Interaction {
                    im: self.im,
                    orm: dispatch::Interaction::new(db, self.log.clone()),
                }
                .find(query, data.into_deserializer())?;

                return Ok(rep);
            }
            Via::Http { .. } => unreachable!(),
        }
    }

    fn _req_http<T, D>(&mut self, path: &str, data: D) -> Result<T>
    where
        D: Serialize,
        for<'de> T: Deserialize<'de>,
    {
        match &self.via {
            Via::Http { url, client } => {
                let vec = serde_cbor::to_vec(&data)?;
                let mut req = hyper::Request::builder()
                    .method("POST")
                    .uri(&url.join_with("/", encode_path(path).to_string()))
                    .body(hyper::Body::from(vec))?;

                req.headers_mut()
                    .insert(ACCEPT, HeaderValue::from_static("application/cbor"));
                req.headers_mut()
                    .insert(CONTENT_TYPE, HeaderValue::from_static("application/cbor"));

                // lol this is async; get fucked simplicity
                use bytes::buf::ext::BufExt;
                let resp: Result<_, Error> = {
                    use tokio::{runtime, task};
                    let mut rt = runtime::Builder::new()
                        .enable_io()
                        .enable_time()
                        .basic_scheduler()
                        .build()
                        .expect("rt");
                    task::LocalSet::new().block_on(&mut rt, async move {
                        let resp = client.request(req).await?;
                        let (head, body) = resp.into_parts();
                        let body = hyper::body::aggregate(body).await?.reader();
                        Ok((head, body))
                    })
                };
                // TODO handle errors or whatever
                let (head, body) = resp?;

                let t: T = serde_cbor::from_reader(body)?;
                Ok(t)
            }
            Via::InProcess { .. } => unreachable!(),
        }
    }

    /// It'd be super cool if we could notice that T isn't a vector or and if we have limit 1
    /// we can submit request with a header or something to return a single item instead of a
    /// vector?
    pub fn find<T, D, E>(&mut self, query: &str, data: D) -> Result<Box<T>>
    where
        for<'de> D: IntoDeserializer<'de, E>,
        D: Serialize,
        for<'de> T: Deserialize<'de>,
        E: serde::de::Error,
        T: 'static,
    {
        match &self.via {
            Via::InProcess { .. } => self
                ._find_via_db(query, data)?
                .content
                .into_any_box()
                .downcast::<T>()
                .map_err(|_| Error::InvalidContainer),
            Via::Http { .. } => self._req_http(query, data),
        }
    }

    pub fn mutate<'i>(
        &mut self,
        changes: &[<<domain::Interaction<'i> as Mutable<'i>>::Change as BorrowChange>::Borrowed],
    ) -> Result<()> {
        match &mut self.via {
            Via::InProcess { ref mut db } => {
                domain::Interaction {
                    im: self.im,
                    orm: dispatch::Interaction::new(db, self.log.clone()),
                }
                .mutate(changes)?;
                return Ok(());
            }
            Via::Http { .. } => self._req_http("", changes),
        }
    }

    /// update one thingy ...
    pub fn update<'i, M>(&mut self, old: M, new: M) -> Result<()>
    where
        Change<M>:
            Into<<<domain::Interaction<'i> as Mutable<'i>>::Change as BorrowChange<'i>>::Borrowed>,
    {
        let change = Change {
            old: Some(old),
            new: Some(new),
        };
        self.mutate(&[change.into()])
    }

    /// insert one thingy ...
    pub fn insert<'i, M>(&mut self, new: M) -> Result<()>
    where
        Change<M>:
            Into<<<domain::Interaction<'i> as Mutable<'i>>::Change as BorrowChange<'i>>::Borrowed>,
    {
        let change = Change {
            old: None,
            new: Some(new),
        };
        self.mutate(&[change.into()])
    }
}
