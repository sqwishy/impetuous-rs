// (todo maybe) remove CursoredGet, and update our signature
// to -> Result<(cols_read, Self::Out)>?

use super::CursoredGet;

pub trait FromColumns {
    type Out;

    fn from_columns(&self, row: &mut CursoredGet) -> rusqlite::Result<Self::Out>
    where
        Self: Sized;

    fn from_row(&self, row: &rusqlite::Row) -> rusqlite::Result<Self::Out>
    where
        Self: Sized,
    {
        Ok(self.from_columns(&mut CursoredGet::from(row))?)
    }
}

impl FromColumns for () {
    type Out = ();

    fn from_columns(&self, _row: &mut CursoredGet) -> rusqlite::Result<Self::Out>
    where
        Self: Sized,
    {
        Ok(())
    }
}
