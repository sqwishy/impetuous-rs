/// todo this really needs a better name than just that of both its variants ...
#[derive(Debug, PartialEq)]
pub enum FewOrMany<T> {
    Few([T; 1]),
    Many(Vec<T>),
}

use FewOrMany::*;

impl<T> FewOrMany<T> {
    pub fn one(t: T) -> Self {
        Few([t])
    }

    pub fn new<I: Iterator<Item = T>>(mut it: I) -> Option<Self> {
        let first = it.next()?;
        if let Some(second) = it.next() {
            let mut v = vec![first, second];
            v.extend(it);
            Some(Many(v))
        } else {
            Some(Self::one(first))
        }
    }

    pub fn push(self, t: T) -> Self {
        match self {
            Few([f]) => Many(vec![f, t]),
            Many(mut v) => {
                v.push(t);
                Many(v)
            }
        }
    }

    pub fn first(&self) -> &T {
        match self {
            Few([t]) => t,
            Many(v) => v.first().unwrap(),
        }
    }

    pub fn first_mut(&mut self) -> &mut T {
        match self {
            Few([t]) => t,
            Many(v) => v.first_mut().unwrap(),
        }
    }
}

#[test]
fn test() {
    let fm = FewOrMany::one(1);
    assert_eq!(fm, FewOrMany::Few([1]));

    let fm = FewOrMany::new(std::iter::empty::<i32>());
    assert_eq!(fm, None);

    let fm = FewOrMany::new(vec![1].into_iter());
    assert_eq!(fm, Some(FewOrMany::Few([1])));

    let fm = FewOrMany::new(vec![1, 2, 3].into_iter());
    assert_eq!(fm, Some(FewOrMany::Many(vec![1, 2, 3])));
}
