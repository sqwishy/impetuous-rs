/// like an option, but can help for deserializing explicit null values.
///
/// TODO this is really hard to work with and I'm somewhat regretting it ...
///
/// Also, Maybe<Option<T>> and Maybe<T> should be handled differently.
/// Only the latter accepts a null value, both can be used to detect omitted values.
///
/// When used in a struct, the fields of this type should probably be annotated
/// with something like the following ...
///
///   #[serde(
///       default = "crate::maybe::maybe_not",
///       deserialize_with = "crate::maybe::deserialize",
///       skip_serializing_if = "Option::is_none"
///   )]
///
/// see also: https://github.com/serde-rs/serde/issues/984
use serde::{Deserialize, Deserializer, Serialize};
use std::fmt;
use std::ops::{Deref, DerefMut};

#[derive(Clone, PartialEq, PartialOrd, Eq, Ord, Hash, Deserialize, Serialize, Default)]
#[repr(transparent)]
pub struct Maybe<T>(Option<T>);

impl<T: fmt::Debug> fmt::Debug for Maybe<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.0 {
            Some(t) => t.fmt(f),
            None => write!(f, "()"),
        }
    }
}

pub fn maybe<T, I: Into<T>>(i: I) -> Maybe<T> {
    Some(i.into()).into()
}

pub fn maybe_not<T>() -> Maybe<T> {
    None.into()
}

impl<T> Deref for Maybe<T> {
    type Target = Option<T>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for Maybe<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl<T> Maybe<T> {
    pub fn into_option(self) -> Option<T> {
        self.0
    }
}

impl<T> From<Option<T>> for Maybe<T> {
    fn from(o: Option<T>) -> Maybe<T> {
        Maybe(o)
    }
}

impl<T> Into<Option<T>> for Maybe<T> {
    fn into(self) -> Option<T> {
        self.into_option()
    }
}

pub trait MaybeExt {
    fn as_str_or<'s>(&'s self, default: &'s str) -> &'s str;
}

impl MaybeExt for Maybe<String> {
    fn as_str_or<'s>(&'s self, fallback: &'s str) -> &'s str {
        self.as_ref().map(|s| s.as_str()).unwrap_or(fallback)
    }
}

pub fn deserialize<'de, T, D>(deserializer: D) -> Result<Maybe<T>, D::Error>
where
    T: Deserialize<'de>,
    D: Deserializer<'de>,
{
    Deserialize::deserialize(deserializer)
        .map(Some)
        .map(From::from)
}

use rusqlite::types::{FromSql, FromSqlResult, ValueRef};

impl<T> FromSql for Maybe<T>
where
    T: FromSql,
{
    fn column_result(value: ValueRef) -> FromSqlResult<Self> {
        T::column_result(value).map(Some).map(From::from)
    }
}

#[test]
fn test_mutability() {
    let mut maybe: Maybe<i32> = None.into();
    (&mut maybe).get_or_insert(123);
    assert_eq!(maybe, Some(123).into());
}

#[test]
fn test_maybe_serde() {
    use serde_json::{self, json};

    #[derive(Debug, PartialEq, Serialize, Deserialize)]
    struct Derp {
        #[serde(
            default,
            deserialize_with = "deserialize",
            skip_serializing_if = "Option::is_none"
        )]
        foo: Maybe<Option<String>>,
        #[serde(
            default,
            deserialize_with = "deserialize",
            skip_serializing_if = "Option::is_none"
        )]
        bar: Maybe<bool>,
    }

    for (json, obj) in vec![
        (
            json!({ "foo": "present" }),
            Derp {
                foo: Some(Some("present".to_owned())).into(),
                bar: None.into(),
            },
        ),
        (
            json!({ "foo": Option::<String>::None }),
            Derp {
                foo: Some(None).into(),
                bar: None.into(),
            },
        ),
        (
            json!({}),
            Derp {
                foo: None.into(),
                bar: None.into(),
            },
        ),
    ] {
        let got: Derp = serde_json::from_value(json.clone()).unwrap();
        assert_eq!(got, obj, "<- expected when deserializing {}", json);

        let got: serde_json::Value = serde_json::to_value(obj).unwrap();
        assert_eq!(got, json.clone(), "<- expected when serializing {}", json);
    }

    match serde_json::from_value::<Derp>(json!({ "bar": Option::<bool>::None })) {
        Err(e)
            if e.to_string()
                .contains("invalid type: null, expected a boolean") =>
        {
            ()
        }
        r @ _ => panic!("wanted deserialization failure, got: {:?}", r),
    };
}
