#![allow(unused_imports)]
//! A library for getting dates and times from englishy kind of strings.
//!
//! Designed to parse ambiguous times, that can be used to find an actual [chrono::DateTime] from
//! some reference point.
use std::{num::NonZeroU8, str::FromStr};

use anyhow::Error;
use chrono::{
    Date, DateTime, Datelike, FixedOffset, LocalResult, NaiveDate, NaiveDateTime, NaiveTime,
    TimeZone, Utc, Weekday,
};

mod parse;

/// Holds the result of parsing some possibly ambiguous date.
///
/// (Implements [FromStr])
///
/// TODO it'd be cool if this worked for any [Datelike], but it's tricky without a Datelike factory
#[derive(Clone, Copy, Default, Debug, PartialEq)]
pub struct Dateish {
    /// Day of the month [1-31]
    day: Option<NonZeroU8>,
    /// Day of week week
    weekday: Option<Weekday>,
    /// Month of the year [1-12]
    month: Option<NonZeroU8>,
    year: Option<i32>,
}

impl From<chrono::NaiveDate> for Dateish {
    fn from(date: chrono::NaiveDate) -> Self {
        Dateish {
            day: Some(NonZeroU8::new(date.day() as u8).expect("non-zero day")),
            month: Some(NonZeroU8::new(date.month() as u8).expect("non-zero month")),
            year: Some(date.year()),
            ..Dateish::default()
        }
    }
}

impl Dateish {
    /// Try to create a new Dateish, it should only be successful if there exists at least one
    /// possible date that satisfiies the conditions that this imposes.
    pub fn new_dwmy(
        day: Option<NonZeroU8>,
        weekday: Option<Weekday>,
        month: Option<NonZeroU8>,
        year: Option<i32>,
    ) -> Result<Self, Error> {
        let ish = Dateish {
            day,
            weekday,
            month,
            year,
        };
        ish.is_possible().map(|()| ish)
    }

    pub fn new_dwmy_unchecked(
        day: Option<NonZeroU8>,
        weekday: Option<Weekday>,
        month: Option<NonZeroU8>,
        year: Option<i32>,
    ) -> Self {
        Dateish {
            day,
            weekday,
            month,
            year,
        }
    }

    pub fn day(&self) -> Option<NonZeroU8> {
        self.day
    }

    pub fn weekday(&self) -> Option<Weekday> {
        self.weekday
    }

    pub fn month(&self) -> Option<NonZeroU8> {
        self.month
    }

    pub fn year(&self) -> Option<i32> {
        self.year
    }

    /// The exact date this must refer to.  Exists if this has a year, month, and day.
    pub fn date(&self) -> Option<NaiveDate> {
        match (self.year, self.month, self.day) {
            (Some(year), Some(month), Some(day)) => {
                NaiveDate::from_ymd_opt(year, month.get() as u32, day.get() as u32)
            }
            _ => None,
        }
    }

    /// The lower bound of valid dates that match this pattern. Exists iff year is some.
    pub fn lower(&self) -> Option<NaiveDate> {
        self.year.and_then(|year| {
            let day = self.day.map(NonZeroU8::get).unwrap_or(1);
            let month = self.month.map(NonZeroU8::get).unwrap_or(1);
            NaiveDate::from_ymd_opt(year, month as u32, day as u32)
        })
    }

    /// The upper bound of valid dates that match this pattern. Exists iff year is some.
    pub fn upper(&self) -> Option<NaiveDate> {
        self.year.and_then(|year| {
            let month = self.month.map(NonZeroU8::get).unwrap_or(12);
            let day = self
                .day
                .map(NonZeroU8::get)
                .map(Some)
                .unwrap_or_else(|| days_in_month(month, Some(year)))?;
            NaiveDate::from_ymd_opt(year, month as u32, day as u32)
        })
    }

    /// Produce an error if this constraint is impossible & no such date exists that satisfies it.
    ///
    /// TODO This should be a ctor to prevent creating invalid instances
    fn is_possible(&self) -> Result<(), Error> {
        match (
            self.month.map(NonZeroU8::get),
            self.day.map(NonZeroU8::get),
            self.year,
        ) {
            (None, None, _) => (),
            (None, Some(day), _) => {
                if day > 31 {
                    return Err(Error::msg("this day does not exist in any month"));
                }
            }
            (Some(month), None, _) => {
                if month > 12 {
                    return Err(Error::msg("this is not a month"));
                }
            }
            (Some(month), Some(day), year) => {
                let max_day =
                    days_in_month(month, year).ok_or(Error::msg("this is not a month"))?;
                if day > max_day {
                    return Err(Error::msg("this day does not exist in this month"));
                }
            }
        }
        Ok(())
    }

    pub fn with_hint<Tz: TimeZone>(&self, hint: &Date<Tz>) -> Option<NaiveDate> {
        NaiveDate::from_ymd_opt(
            self.year.unwrap_or(hint.year()),
            self.month.map(|v| v.get() as u32).unwrap_or(hint.month()),
            self.day.map(|v| v.get() as u32).unwrap_or(hint.day()),
        )
    }
}

/// A structure holding the result of parsing something time and/or date-ish.
///
/// (Implements [FromStr])
#[derive(Clone, Copy, Default, Debug, PartialEq)]
pub struct DateishTime {
    pub time: Option<NaiveTime>,
    pub date: Dateish,
    pub offset: Option<FixedOffset>,
}

impl From<Dateish> for DateishTime {
    fn from(date: Dateish) -> Self {
        DateishTime {
            date,
            ..DateishTime::default()
        }
    }
}

impl From<chrono::NaiveDateTime> for DateishTime {
    fn from(dt: chrono::NaiveDateTime) -> Self {
        DateishTime {
            date: dt.date().into(),
            time: Some(dt.time()),
            ..DateishTime::default()
        }
    }
}

impl FromStr for DateishTime {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use nom::{
            branch::alt,
            character::complete::multispace0,
            combinator::{all_consuming, map, opt},
            error::{context, convert_error, VerboseError},
            sequence::tuple,
        };
        use parse::parse_dateishtime;

        all_consuming(parse_dateishtime)(s.trim())
            .map(|(rem, tdish)| {
                assert!(rem.is_empty(), rem.to_owned());
                tdish
            })
            .map_err(|e: nom::Err<VerboseError<&str>>| match e {
                nom::Err::Error(e) | nom::Err::Failure(e) => Error::msg(convert_error(s, e)),
                nom::Err::Incomplete(_) => unreachable!(),
            })
    }
}

impl<'de> serde::de::Deserialize<'de> for DateishTime {
    fn deserialize<D>(deserializer: D) -> Result<DateishTime, D::Error>
    where
        D: serde::de::Deserializer<'de>,
    {
        use std::{fmt, str::FromStr};

        struct FromStrVisitor;

        impl<'de> serde::de::Visitor<'de> for FromStrVisitor {
            type Value = DateishTime;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("stringy date and time")
            }

            fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                FromStr::from_str(&s).map_err(serde::de::Error::custom)
            }
        }

        deserializer.deserialize_str(FromStrVisitor)
    }
}

impl DateishTime {
    pub fn local_today() -> Self {
        let day = chrono::Local::today();
        DateishTime {
            date: day.naive_local().into(),
            time: None,
            offset: Some(*day.offset()),
        }
    }

    pub fn local_now() -> Self {
        let now = chrono::Local::now();
        DateishTime {
            date: now.naive_local().date().into(),
            time: Some(now.naive_local().time()),
            offset: Some(*now.offset()),
        }
    }

    pub fn local_yesterday() -> Self {
        let day = chrono::Local::today().pred();
        DateishTime {
            date: day.naive_local().into(),
            time: None,
            offset: Some(*day.offset()),
        }
    }

    pub fn local_tomorrow() -> Self {
        let day = chrono::Local::today().succ();
        DateishTime {
            date: day.naive_local().into(),
            time: None,
            offset: Some(*day.offset()),
        }
    }

    /// If a date and time were extracted pretty unambigiously, return it.
    pub fn datetime(&self) -> Option<NaiveDateTime> {
        match (self.date.date(), self.time) {
            (Some(date), Some(time)) => Some(date.and_time(time)),
            _ => None,
        }
    }

    /// Similar to [datetime()] but treats naive times as UTC. Uses a parsed offset if possible.
    pub fn datetime_utc(&self) -> Option<DateTime<Utc>> {
        self.datetime().map(|dt| {
            if let Some(offset) = self.offset {
                Utc.from_utc_datetime(&(dt - offset))
            } else {
                Utc.from_utc_datetime(&dt)
            }
        })
    }

    /// Similar to [datetime()] but tries to convert the input into a time zone
    /// aware time either using an offset parsed from the string or as local time.
    pub fn datetime_with_tz<Tz: TimeZone>(
        &self,
        tz: &Tz,
    ) -> Option<chrono::LocalResult<DateTime<Tz>>> {
        self.datetime().and_then(|dt| {
            if let Some(offset) = self.offset {
                let utc = dt - offset;
                Some(chrono::LocalResult::Single(tz.from_utc_datetime(&utc)))
            } else {
                Some(tz.from_local_datetime(&dt))
            }
        })
    }

    /// Try to build a timezone aware [DateTime] from the DateishTime, falling back to
    /// using values given in in the `fallback`'s date, time, and timezone when that
    /// information is missing from the DateishTime.
    ///
    /// This can still fail if say the DateishTime is "Feb 29th" and the fallback is not
    /// a leap year.
    ///
    /// TODO, if the dateish has a weekday but no day, it will look around for the
    /// closest day to `fallback` on its weekday. It ignores any hints from the month or
    /// year. But that's mostly okay because I don't think there are strings that can
    /// parse into this type that contains a weekday and either a month or year with no
    /// day of month...
    pub fn datetime_with_hint<Tz: TimeZone>(
        &self,
        hint: DateTime<Tz>,
    ) -> chrono::LocalResult<DateTime<Tz>> {
        let Dateish { day, weekday, .. } = self.date;

        let mut hint = hint;

        if let Some(weekday) = weekday {
            if day.is_none() {
                // The user can specify a weekday instead of the day ...
                // ... find the nearest date to hint on that day of the week.
                let near = nearest_by_weekday(hint.date(), weekday)
                    .and_then(|date| date.and_time(hint.time()));
                match near {
                    Some(date) => hint = date,
                    None => return chrono::LocalResult::None,
                }
            }
        }

        if let Some(offset) = self.offset {
            let off_hint = hint.with_timezone(&offset);
            self.with_hint(&off_hint.date(), hint.time())
                .map(|dt| dt.with_timezone(&hint.timezone()))
        } else {
            self.with_hint(&hint.date(), hint.time())
        }
    }

    /// As [datetime_with_fallback] but only uses a date as a hint.
    ///
    /// Returns None if there is no time information.
    pub fn datetime_with_date_hint<Tz: TimeZone>(
        &self,
        hint: Date<Tz>,
    ) -> Option<chrono::LocalResult<DateTime<Tz>>> {
        let Dateish { day, weekday, .. } = self.date;

        let time = self.time?;

        let mut hint = hint;

        if let Some(weekday) = weekday {
            if day.is_none() {
                // The user can specify a weekday instead of the day ...
                // ... find the nearest date to fallback on that day of the week.
                hint = match nearest_by_weekday(hint, weekday) {
                    Some(date) => date,
                    None => return Some(chrono::LocalResult::None),
                };
            }
        }

        if let Some(offset) = self.offset {
            let off_hint = hint.with_timezone(&offset);
            self.with_hint(&off_hint, time)
                .map(|dt| dt.with_timezone(&hint.timezone()))
        } else {
            self.with_hint(&hint, time)
        }
        .into()
    }

    fn with_hint<Tz: TimeZone>(
        &self,
        date: &Date<Tz>,
        time: NaiveTime,
    ) -> LocalResult<DateTime<Tz>> {
        self.date
            .with_hint(date)
            .map(|date| date.and_time(self.time.unwrap_or(time)))
            .map(|dt| date.timezone().from_local_datetime(&dt))
            .unwrap_or(chrono::LocalResult::None)
    }
}

/// Rougly the number of days in the given month (1 is Jan).  Feb has 29 days.
fn days_in_month(month: u8, year: Option<i32>) -> Option<u8> {
    match month {
        1 | 3 | 5 | 7 | 8 | 10 | 12 => Some(31),
        2 => {
            if Some(false) == year.map(is_leap_year) {
                Some(28)
            } else {
                Some(29)
            }
        }
        4 | 6 | 9 | 11 => Some(30),
        _ => None,
    }
}

fn is_leap_year(year: i32) -> bool {
    NaiveDate::from_ymd_opt(year, 2, 29).is_some()
}

/// Find the nearest date to the given NaiveDate on the given weekday.
///
/// Should only return None in the event of an overflow.
fn nearest_by_weekday<Tz>(near: Date<Tz>, weekday: Weekday) -> Option<Date<Tz>>
where
    Tz: TimeZone,
{
    if near.weekday() == weekday {
        return Some(near);
    }

    let from = near.weekday().num_days_from_monday() as i8;
    let want = weekday.num_days_from_monday() as i8;

    // weekdays can't be more than 3 days from any other weekday
    let difference = match want - from {
        n if n > 3 => n - 7,
        n if n < -3 => n + 7,
        n => n,
    };

    assert!(difference < 4, "{} {}; {} >= 4", want, from, difference);

    // The impl for this seems super crazy. Complicated than succ_opt() or the
    // Datelike::with_ordinal0 but ... idk maybe I'm wrong or maybe using those is
    // wrong, but it's possible that this is doing more work than it needs to?
    return near.checked_add_signed(chrono::Duration::days(difference.into()));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_things_that_should_work() -> anyhow::Result<()> {
        let monday_4pm = DateishTime {
            time: Some(NaiveTime::from_hms(16, 00, 0)),
            date: Dateish {
                weekday: Some(Weekday::Mon),
                ..Dateish::default()
            },
            ..DateishTime::default()
        };
        assert_eq!(monday_4pm, "16:00:00 Mon".parse::<DateishTime>()?);
        assert_eq!(monday_4pm, "Monday 4pm".parse::<DateishTime>()?);

        // let t = DateishTime {
        //     time: Some(four_pm),
        //     date: Dateish::default(),
        //     ..DateishTime::default()
        // };
        // assert_eq!(t, "16:00".parse::<DateishTime>()?);

        Ok(())
    }

    #[test]
    fn test_nearest_by_weekday() {
        let pt = chrono_tz::Canada::Pacific;
        [
            (Weekday::Mon, Some(pt.ymd(2020, 2, 3))),
            (Weekday::Tue, Some(pt.ymd(2020, 2, 4))),
            (Weekday::Wed, Some(pt.ymd(2020, 2, 5))),
            (Weekday::Thu, Some(pt.ymd(2020, 2, 6))),
            (Weekday::Fri, Some(pt.ymd(2020, 1, 31))),
            (Weekday::Sat, Some(pt.ymd(2020, 2, 1))),
            (Weekday::Sun, Some(pt.ymd(2020, 2, 2))),
        ]
        .iter()
        .for_each(|(weekday, expected)| {
            let got = nearest_by_weekday(pt.ymd(2020, 2, 3), *weekday);
            assert_eq!(
                got, *expected,
                "nearest {} was {:?} instead of {:?}",
                weekday, got, expected
            );
        });
    }

    #[test]
    fn test_meme() -> anyhow::Result<()> {
        //     January 2020          February 2020          March 2020
        // Mo Tu We Th Fr Sa Su  Mo Tu We Th Fr Sa Su  Mo Tu We Th Fr Sa Su
        //        1  2  3  4  5                  1  2                     1
        //  6  7  8  9 10 11 12   3  4  5  6  7  8  9   2  3  4  5  6  7  8
        // 13 14 15 16 17 18 19  10 11 12 13 14 15 16   9 10 11 12 13 14 15
        // 20 21 22 23 24 25 26  17 18 19 20 21 22 23  16 17 18 19 20 21 22
        // 27 28 29 30 31        24 25 26 27 28 29     23 24 25 26 27 28 29

        let pt = chrono_tz::Canada::Pacific;
        let feb_2nd = pt.ymd(2020, 2, 2);
        let feb_2nd_noon = feb_2nd.and_hms(12, 0, 0);
        let feb_8th_noon = pt.ymd(2020, 2, 8).and_hms(12, 0, 0);
        let feb_8th_6pm = pt.ymd(2020, 2, 8).and_hms(18, 0, 0);
        let jan_30th_6pm = pt.ymd(2020, 1, 30).and_hms(18, 0, 0);

        let ish = "Feb 8".parse::<DateishTime>()?;
        let result = ish.datetime_with_hint(feb_2nd_noon);
        assert_eq!(result.single(), Some(feb_8th_noon));

        let ish = "Feb 8 18".parse::<DateishTime>()?;
        let result = ish.datetime_with_hint(feb_2nd_noon);
        assert_eq!(result.single(), Some(feb_8th_6pm));

        let ish = "Thu 18:00".parse::<DateishTime>()?;
        let result = ish.datetime_with_hint(feb_2nd_noon);
        assert_eq!(result.single(), Some(jan_30th_6pm));

        let ish = "Thursday, Jan 30 2020 18:00".parse::<DateishTime>()?;
        let result = ish.datetime_with_hint(feb_2nd_noon);
        assert_eq!(result.single(), Some(jan_30th_6pm));

        let ish = "18:00 Thursday, Jan 30 2020".parse::<DateishTime>()?;
        let result = ish.datetime_with_hint(feb_2nd_noon);
        assert_eq!(result.single(), Some(jan_30th_6pm));

        Ok(())
    }

    #[test]
    fn test_pst_to_pdt() -> anyhow::Result<()> {
        let pt = chrono_tz::Canada::Pacific;
        let nov_1st_0030_pdt = pt
            .from_local_datetime(&NaiveDate::from_ymd(2020, 11, 1).and_hms(0, 30, 0))
            .single()
            .unwrap();
        let nov_1st_0130_pdt = pt
            .from_local_datetime(&NaiveDate::from_ymd(2020, 11, 1).and_hms(1, 30, 0))
            .earliest()
            .unwrap();
        let nov_1st_0130_pst = pt
            .from_local_datetime(&NaiveDate::from_ymd(2020, 11, 1).and_hms(1, 30, 0))
            .latest()
            .unwrap();
        let nov_1st_0230_pst = pt
            .from_local_datetime(&NaiveDate::from_ymd(2020, 11, 1).and_hms(2, 30, 0))
            .single()
            .unwrap();

        [
            ("0:30 -0700", Some(nov_1st_0030_pdt)),
            ("1:30 -0700", Some(nov_1st_0130_pdt)),
            ("1:30 -0800", Some(nov_1st_0130_pst)),
            ("2:30 -0800", Some(nov_1st_0230_pst)),
        ]
        .iter()
        .cloned()
        .try_for_each(|(inp, exp)| -> anyhow::Result<()> {
            // This fails for 1:30 -0800; being at 0:30 it thinks we mean yesterday (oct 31st)
            // let result = inp
            //     .parse::<DateishTime>()?
            //     .datetime_with_hint(nov_1st_0030_pdt);
            // assert_eq!(result.single(), exp, "{:?} / {:?}", inp, nov_1st_0030_pdt);

            let result = inp
                .parse::<DateishTime>()?
                .datetime_with_hint(nov_1st_0130_pdt);
            assert_eq!(result.single(), exp, "{:?} / {:?}", inp, nov_1st_0130_pdt);

            let result = inp
                .parse::<DateishTime>()?
                .datetime_with_hint(nov_1st_0130_pst);
            assert_eq!(result.single(), exp, "{:?} / {:?}", inp, nov_1st_0130_pst);

            let result = inp
                .parse::<DateishTime>()?
                .datetime_with_hint(nov_1st_0230_pst);
            assert_eq!(result.single(), exp, "{:?} / {:?}", inp, nov_1st_0230_pst);

            Ok(())
        })
    }

    #[test]
    fn test_dateish_datetime_with_tz() -> anyhow::Result<()> {
        use chrono::LocalResult;

        let pt = chrono_tz::Canada::Pacific;

        let ish = "Nov 1 2020 1:30".parse::<DateishTime>()?;
        let res = ish.datetime_with_tz(&pt).unwrap();
        assert!(match res {
            LocalResult::Ambiguous(_, _) => true,
            _ => false,
        });

        let ish = "Nov 1 2020 1:30 -0700".parse::<DateishTime>()?;
        let res = ish.datetime_with_tz(&pt).unwrap();
        assert_eq!(ish.offset, Some(FixedOffset::west(7 * 3600)));
        assert!(res.single().is_some());

        let ish = "Nov 1 2020 1:30 -0800".parse::<DateishTime>()?;
        let res = ish.datetime_with_tz(&pt).unwrap();
        assert_eq!(ish.offset, Some(FixedOffset::west(8 * 3600)));
        assert!(res.single().is_some());

        Ok(())
    }
}
