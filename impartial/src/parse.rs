use std::num::NonZeroU8;

use chrono::{FixedOffset, NaiveTime, Weekday};

use nom::{
    branch::{alt, permutation},
    bytes::complete::{tag_no_case, take},
    character::complete::{alpha1 as alpha, char, digit1 as digit, multispace0 as ws},
    combinator::{cond, map, map_opt, map_res, not, opt, peek},
    error::{context, ParseError},
    sequence::{preceded, terminated, tuple},
    IResult,
};

use crate::{Dateish, DateishTime};

pub fn scream_into_the_void<A, B>(v: Option<(A, Option<B>)>) -> (Option<A>, Option<B>) {
    v.map(|(a, maybe_b)| (Some(a), maybe_b))
        .unwrap_or((None, None))
}

pub fn parse_u32<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, u32, E> {
    context("u32", map_res(digit, |n: &str| n.parse::<u32>()))(i)
}

pub fn parse_u8<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, u8, E> {
    context("u8", map_res(digit, |n: &str| n.parse::<u8>()))(i)
}

pub fn thstndrd<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, (), E> {
    map_opt(take(2u8), |i: &str| match i {
        "th" | "TH" | "Th" | "tH" | "st" | "ST" | "St" | "sT" | "nd" | "ND" | "Nd" | "nD"
        | "rd" | "RD" | "Rd" | "rD" => Some(()),
        _ => None,
    })(i)
}

/// optional comma followed by 0 or more whitespace (also matches nothing)
fn comma_ws<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, (), E> {
    map(tuple((opt(char(',')), ws)), |_| ())(i)
}

enum Hand {
    Am,
    Pm,
}

/// hour[[:]minute[:second]][.ms]
fn hms<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, (u32, u32, u32, u32), E> {
    map(
        tuple((
            parse_u32,
            opt(tuple((
                preceded(char(':'), parse_u32),
                opt(preceded(char(':'), parse_u32)),
            ))),
            opt(preceded(char('.'), parse_u32)),
        )),
        |(h, ms, milli)| {
            let (m, s) = ms
                .map(|(m, s)| (m, s.unwrap_or(0u32)))
                .unwrap_or((0u32, 0u32));
            (h, m, s, milli.unwrap_or(0u32))
        },
    )(i)
}

fn ampm<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Hand, E> {
    map_opt(take(2u8), |i: &str| match i {
        "am" | "AM" | "Am" | "aM" => Some(Hand::Am),
        "pm" | "PM" | "Pm" | "pM" => Some(Hand::Pm),
        _ => None,
    })(i)
}

/// -NN[:][NN]
pub fn offset<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, chrono::FixedOffset, E> {
    enum Sign {
        East,
        West,
    };

    map_opt(
        tuple((
            alt((
                map(char('-'), |_| Sign::West),
                map(char('+'), |_| Sign::East),
            )),
            map_res(take(2u8), str::parse),
            opt(preceded(opt(char(':')), map_res(take(2u8), str::parse))),
        )),
        |(sign, h, m): (Sign, u8, Option<u8>)| {
            const SECS_PER_HOUR: i32 = 3600;
            const SECS_PER_MIN: i32 = 60;

            let mut s = (h as i32).checked_mul(SECS_PER_HOUR)?;

            let m = m.unwrap_or(0);
            if m > 0 {
                s = s.checked_add((m as i32).checked_mul(SECS_PER_MIN)?)?;
            }

            match sign {
                Sign::East => FixedOffset::east_opt(s),
                Sign::West => FixedOffset::west_opt(s),
            }
        },
    )(i)
}

/// hour[[:]minute[:second]] [am|pm]
pub fn time<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, NaiveTime, E> {
    map_opt(
        tuple((hms, opt(preceded(ws, ampm)))),
        |((h, m, s, milli), ampm)| match ampm {
            // pm usually just adds 12 to the hours, except when the hour is 12 we do nothing
            Some(Hand::Pm) if h == 12 => NaiveTime::from_hms_milli_opt(12, m, s, milli),
            Some(Hand::Pm) => NaiveTime::from_hms_milli_opt(h + 12, m, s, milli),
            // am usually does nothing to the hours, except when the hour is 12 we subtract 12
            Some(Hand::Am) if h > 12 => None,
            Some(Hand::Am) if h == 12 => NaiveTime::from_hms_milli_opt(0, m, s, milli),
            Some(Hand::Am) => NaiveTime::from_hms_milli_opt(h, m, s, milli),
            // twenty-four hour time? super simple stuff
            None => NaiveTime::from_hms_milli_opt(h, m, s, milli),
        },
    )(i)
}

/// hour:minute[:second]] [am|pm]
/// hour[:minute[:second]] am|pm
pub fn unambiguous_time<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, NaiveTime, E> {
    // this is basically hms but the minute is required?
    let unambig_hms = tuple((
        parse_u32,
        preceded(char(':'), parse_u32),
        map(
            opt(preceded(char(':'), parse_u32)),
            Option::unwrap_or_default,
        ),
        map(
            opt(preceded(char('.'), parse_u32)),
            Option::unwrap_or_default,
        ),
    ));

    context(
        "unambiguous time",
        map_opt(
            alt((
                tuple((unambig_hms, opt(preceded(ws, ampm)))),
                tuple((hms, map(preceded(ws, ampm), Some))),
            )),
            |((h, m, s, milli), ampm)| match ampm {
                Some(Hand::Am) if h > 12 => None,
                Some(Hand::Pm) => NaiveTime::from_hms_milli_opt(h + 12, m, s, milli),
                _ => NaiveTime::from_hms_milli_opt(h, m, s, milli),
            },
        ),
    )(i)
}

///
pub fn time_with_offset<'a, E: ParseError<&'a str>>(
    i: &'a str,
) -> nom::IResult<&'a str, (NaiveTime, Option<FixedOffset>), E> {
    tuple((time, opt(preceded(ws, offset))))(i)
}

fn non_zero_u8<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, NonZeroU8, E> {
    map_opt(parse_u8, NonZeroU8::new)(i)
}

fn weekday<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Weekday, E> {
    map_res(alpha, |s: &str| s.parse::<Weekday>())(i)
}

fn month_by_name<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, NonZeroU8, E> {
    let jan = tuple((tag_no_case("jan"), opt(tag_no_case("uary"))));
    let feb = tuple((tag_no_case("feb"), opt(tag_no_case("ruary"))));
    let mar = tuple((tag_no_case("mar"), opt(tag_no_case("ch"))));
    let apr = tuple((tag_no_case("apr"), opt(tag_no_case("il"))));
    let may = tag_no_case("may");
    let jun = tuple((tag_no_case("jun"), opt(tag_no_case("e"))));
    let jul = tuple((tag_no_case("jul"), opt(tag_no_case("y"))));
    let aug = tuple((tag_no_case("aug"), opt(tag_no_case("ust"))));
    let sep = tuple((tag_no_case("sep"), opt(tag_no_case("tember"))));
    let oct = tuple((tag_no_case("oct"), opt(tag_no_case("ober"))));
    let nov = tuple((tag_no_case("nov"), opt(tag_no_case("emver"))));
    let dec = tuple((tag_no_case("dec"), opt(tag_no_case("ember"))));

    alt((
        map(jan, |_| unsafe { NonZeroU8::new_unchecked(1) }),
        map(feb, |_| unsafe { NonZeroU8::new_unchecked(2) }),
        map(mar, |_| unsafe { NonZeroU8::new_unchecked(3) }),
        map(apr, |_| unsafe { NonZeroU8::new_unchecked(4) }),
        map(may, |_| unsafe { NonZeroU8::new_unchecked(5) }),
        map(jun, |_| unsafe { NonZeroU8::new_unchecked(6) }),
        map(jul, |_| unsafe { NonZeroU8::new_unchecked(7) }),
        map(aug, |_| unsafe { NonZeroU8::new_unchecked(8) }),
        map(sep, |_| unsafe { NonZeroU8::new_unchecked(9) }),
        map(oct, |_| unsafe { NonZeroU8::new_unchecked(10) }),
        map(nov, |_| unsafe { NonZeroU8::new_unchecked(11) }),
        map(dec, |_| unsafe { NonZeroU8::new_unchecked(12) }),
    ))(i)
}

fn month_by_n<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, NonZeroU8, E> {
    map_opt(parse_u8, NonZeroU8::new)(i)
}

fn month<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, NonZeroU8, E> {
    alt((month_by_name, month_by_n))(i)
}

/// Only captures 4-digit years to disambiguate from days
fn year4<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, i32, E> {
    context("year4", map_res(take(4u8), |n: &str| n.parse::<i32>()))(i)
}

/// 31st
fn unambiguous_day<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, NonZeroU8, E> {
    terminated(non_zero_u8, thstndrd)(i)
}

/// 12/31/2020
fn dateish_slashy<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Dateish, E> {
    map_res(
        tuple((
            month,
            preceded(char('/'), non_zero_u8),
            preceded(char('/'), year4),
        )),
        |(month, day, year)| Dateish::new_dwmy(Some(day), None, Some(month), Some(year)),
    )(i)
}

/// 2020-01-31
fn dateish_hyphenated<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Dateish, E> {
    map_res(
        tuple((
            year4,
            preceded(char('-'), month),
            preceded(char('-'), non_zero_u8),
        )),
        |(year, month, day)| Dateish::new_dwmy(Some(day), None, Some(month), Some(year)),
    )(i)
}

/// Fri Jan 31[st] 2020 -- all the things are optional but at least one must match and day can come before the month
fn dateish_weekday_mdy<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Dateish, E> {
    map_res(
        |i| {
            // Weekday
            let (i, weekday) = opt(weekday)(i)?;

            // First month
            let pre = cond(weekday.is_some(), comma_ws);
            let (i, month) = opt(preceded(pre, month_by_name))(i)?;

            // Day of the month. To ensure we don't match a time, we try to match
            // unambiguous_time first and bail if that matched I suppose...
            let pre = cond(weekday.is_some() || month.is_some(), comma_ws);

            let (i, day) = opt(preceded(
                tuple((pre, not(unambiguous_time))),
                terminated(non_zero_u8, opt(thstndrd)),
            ))(i)?;

            // try the month again if we didn't find it but we found a day
            let (i, late_month) = cond(
                month.is_none() && day.is_some(),
                opt(preceded(comma_ws, month_by_name)),
            )(i)?;
            let late_month = late_month.flatten();

            let matched_anything =
                weekday.is_some() || month.is_some() || day.is_some() || late_month.is_some();
            let (i, year) = if matched_anything {
                opt(preceded(comma_ws, year4))(i)?
            } else {
                map(year4, Some)(i)?
            };

            let month = month.or(late_month);
            Ok((i, Dateish::new_dwmy(day, weekday, month, year)))
        },
        |res| res,
    )(i)
}

/// month_by_name [year]  (not to be used on its own, should follow a day)
fn month_year<'a, E: ParseError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, (NonZeroU8, Option<i32>), E> {
    map(
        tuple((month_by_name, opt(preceded(ws, year4)))),
        |(month, maybe_year)| (month, maybe_year),
    )(i)
}

pub fn unambiguous_date<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Dateish, E> {
    context(
        "unambiguous date",
        alt((
            dateish_slashy,
            dateish_hyphenated,
            dateish_weekday_mdy,
            map_res(
                tuple((unambiguous_day, opt(preceded(comma_ws, month_year)))),
                |(day, month_year)| {
                    let (month, year) = scream_into_the_void(month_year);
                    Dateish::new_dwmy(Some(day), None, month, year)
                },
            ),
        )),
    )(i)
}

pub fn parse_dateishtime<'a, E: ParseError<&'a str>>(
    i: &'a str,
) -> IResult<&'a str, DateishTime, E> {
    alt((
        // time [offset][,] unambiguous_date
        map(
            tuple((time_with_offset, preceded(comma_ws, unambiguous_date))),
            |((time, offset), date)| DateishTime {
                date: date,
                time: Some(time),
                offset,
            },
        ),
        // unambiguous_date[,] [time [offset]]
        map(
            tuple((unambiguous_date, opt(preceded(comma_ws, time_with_offset)))),
            |(date, time_offset)| {
                let (time, offset) = scream_into_the_void(time_offset);
                DateishTime { date, time, offset }
            },
        ),
        // unambiguous_time [offset] [date?]
        map(
            tuple((
                unambiguous_time,
                opt(preceded(ws, offset)),
                opt(preceded(comma_ws, unambiguous_date)),
            )),
            |(time, offset, date)| DateishTime {
                time: Some(time),
                offset,
                date: date.unwrap_or_default(),
            },
        ),
    ))(i)
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::DateishTime;
    use nom::combinator::all_consuming;
    use nom::error::VerboseError;

    macro_rules! non_zero_u8 {
        ($n:tt) => {
            unsafe { NonZeroU8::new_unchecked($n) }
        };
    }

    const FEB_1ST_2020: Dateish = Dateish {
        day: Some(non_zero_u8!(1)),
        month: Some(non_zero_u8!(2)),
        year: Some(2020),
        weekday: None,
    };

    #[test]
    fn test_dateishtime() -> anyhow::Result<()> {
        let parse_dateishtime =
            |s| all_consuming(parse_dateishtime::<VerboseError<&str>>)(s).map(|(_, nt)| nt);

        let feb_1st_2020_3am = DateishTime {
            date: FEB_1ST_2020,
            time: Some(NaiveTime::from_hms(3, 0, 0)),
            offset: None,
        };

        assert_eq!(feb_1st_2020_3am, parse_dateishtime("2020-2-1 3")?);
        assert_eq!(feb_1st_2020_3am, parse_dateishtime("Feb/1/2020 3am")?);
        assert_eq!(feb_1st_2020_3am, parse_dateishtime("3:00 2/1/2020")?);

        parse_dateishtime("Sat, Feb 1 2020 03:00 -0800")?;
        parse_dateishtime("Sat, 1 Feb 2020 03:00 -0800")?;

        let thu_1pm = DateishTime {
            date: Dateish {
                weekday: Some(Weekday::Thu),
                ..Dateish::default()
            },
            time: Some(NaiveTime::from_hms(13, 0, 0)),
            ..DateishTime::default()
        };

        assert_eq!(thu_1pm, parse_dateishtime("Thu 13:00")?);
        // This isn't supported, probably 24 hour time can be like 13h or something
        // though instead of am/pm
        // assert_eq!(thu_1pm, parse_dateishtime("Thu 1300")?);
        // assert_eq!(thu_1pm, parse_dateishtime("1300 Thu")?);

        let one_am_2nd = DateishTime {
            date: Dateish {
                day: Some(non_zero_u8!(2)),
                ..Dateish::default()
            },
            time: Some(NaiveTime::from_hms(1, 0, 0)),
            ..DateishTime::default()
        };
        assert_eq!(one_am_2nd, parse_dateishtime("1 2nd")?);
        assert_eq!(one_am_2nd, parse_dateishtime("1am 2")?);

        let one_pm_3rd = DateishTime {
            date: Dateish {
                day: Some(non_zero_u8!(3)),
                ..Dateish::default()
            },
            time: Some(NaiveTime::from_hms(13, 0, 0)),
            ..DateishTime::default()
        };
        assert_eq!(one_pm_3rd, parse_dateishtime("1pm 3")?);
        assert_eq!(one_pm_3rd, parse_dateishtime("13 3rd")?);
        // assert_eq!(one_pm_3rd, parse_dateishtime("1300 2")?);
        assert_eq!(one_pm_3rd, parse_dateishtime("13:00 3")?);

        let feb_1st_2020_1020pm = DateishTime {
            date: FEB_1ST_2020,
            time: Some(NaiveTime::from_hms(20, 20, 0)),
            ..DateishTime::default()
        };
        assert_eq!(
            feb_1st_2020_1020pm,
            parse_dateishtime("20:20 1st Feb 2020")?
        );
        assert_eq!(
            DateishTime::from(FEB_1ST_2020),
            parse_dateishtime("1st Feb 2020")?
        );

        Ok(())
    }

    #[test]
    fn test_unambiguous_date() -> anyhow::Result<()> {
        let unambiguous_date = |s| unambiguous_date::<VerboseError<&str>>(s).map(|(_, nt)| nt);
        assert_eq!(FEB_1ST_2020, unambiguous_date("2020-2-1")?);
        assert_eq!(FEB_1ST_2020, unambiguous_date("Feb/1/2020")?);
        assert_eq!(FEB_1ST_2020, unambiguous_date("2/1/2020")?);
        Ok(())
    }

    #[test]
    fn test_time() {
        let time = |s| time::<VerboseError<&str>>(s).map(|(_, nt)| nt).unwrap();
        assert_eq!(time("2"), NaiveTime::from_hms(2, 0, 0));
        assert_eq!(time("1 pm"), NaiveTime::from_hms(13, 0, 0));
        assert_eq!(time("2pm"), NaiveTime::from_hms(14, 0, 0));
        assert_eq!(time("23:30"), NaiveTime::from_hms(23, 30, 0));
        assert_eq!(time("23:30.123"), NaiveTime::from_hms_milli(23, 30, 0, 123));
        assert_eq!(time("1:2:35 AM"), NaiveTime::from_hms(1, 2, 35));
        assert_eq!(time("12:30am"), NaiveTime::from_hms(0, 30, 0));
        assert_eq!(time("12:30pm"), NaiveTime::from_hms(12, 30, 0));
    }

    #[test]
    fn test_offset() -> anyhow::Result<()> {
        use chrono::FixedOffset;
        let offset = |s| offset::<VerboseError<&str>>(s).map(|(_, nt)| nt);
        let hour = 3600;
        assert_eq!(FixedOffset::east(5 * hour), offset("+05")?);
        assert_eq!(FixedOffset::west(7 * hour), offset("-0700")?);
        assert_eq!(FixedOffset::west(8 * hour), offset("-08:00")?);
        Ok(())
    }

    #[test]
    fn test_month() {
        let parse_month = |s| month::<VerboseError<&str>>(s).map(|(_, v)| v);
        assert_eq!(parse_month("January"), Ok(non_zero_u8!(1)));
        assert_eq!(parse_month("feb"), Ok(non_zero_u8!(2)));
        assert_eq!(parse_month("fEbUaRy"), Ok(non_zero_u8!(2)));
        assert_eq!(parse_month("MARCH"), Ok(non_zero_u8!(3)));
        assert_eq!(parse_month("april"), Ok(non_zero_u8!(4)));
        assert_eq!(parse_month("May"), Ok(non_zero_u8!(5)));
        assert_eq!(parse_month("june"), Ok(non_zero_u8!(6)));
        assert_eq!(parse_month("july"), Ok(non_zero_u8!(7)));
        assert_eq!(parse_month("August"), Ok(non_zero_u8!(8)));
        assert_eq!(parse_month("SeptembeR"), Ok(non_zero_u8!(9)));
        assert_eq!(parse_month("OcToBeR"), Ok(non_zero_u8!(10)));
        assert_eq!(parse_month("November"), Ok(non_zero_u8!(11)));
        assert_eq!(parse_month("December"), Ok(non_zero_u8!(12)));
    }
}
