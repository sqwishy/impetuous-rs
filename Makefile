VERSION := $(shell rpm --specfile misc/impetuous.spec -q --queryformat "%{VERSION}\n" | head -n1)
RELEASE := $(shell rpm --specfile misc/impetuous.spec -q --queryformat "%{RELEASE}\n" | head -n1)
RPMBUILD = ~/rpmbuild

SRC = $(shell find \
	cli/src \
	impress/src \
	impartial/src \
	imql/src \
	-type f -name "*.rs")
SRC += $(shell find sql -type f -name "*.sql")
SRC += cli/Cargo.toml \
	cli/Cargo.lock \
	impress/Cargo.toml \
	imql/Cargo.toml \
	impartial/Cargo.toml

.PHONY: rpm srpm sources

rpm: ${RPMBUILD}/RPMS/x86_64/impetuous-${VERSION}-${RELEASE}.x86_64.rpm

${RPMBUILD}/RPMS/x86_64/impetuous-${VERSION}-${RELEASE}.x86_64.rpm: ${RPMBUILD}/SRPMS/impetuous-${VERSION}-${RELEASE}.src.rpm
	rpmbuild --rebuild $^

srpm: ${RPMBUILD}/SRPMS/impetuous-${VERSION}-${RELEASE}.src.rpm

${RPMBUILD}/SRPMS/impetuous-${VERSION}-${RELEASE}.src.rpm: misc/impetuous.spec | sources
	rpmbuild -bs $^

sources: ${RPMBUILD}/SOURCES/impetuous-${VERSION}.tar.gz

# TODO `cargo fetch` git clones https://github.com/rust-lang/crates.io-index.git and all
# its history; it's like 70-something MiB of mildly compressed JSON. It inflates the
# source package quite a bit and it's the stupidest fucking thing.
${RPMBUILD}/SOURCES/impetuous-${VERSION}.tar.gz: ${SRC}
	CARGO_HOME=/tmp/impetuous-${VERSION}/node_modules \
			   cargo fetch --manifest-path cli/Cargo.toml
	mkdir -p ${RPMBUILD}/SOURCES
	tar --transform "s//impetuous-${VERSION}\//" $^ \
		-C /tmp/impetuous-${VERSION} \
		--exclude 'node_modules/registry/src/*/winapi-*' \
		node_modules \
		-czf ${RPMBUILD}/SOURCES/impetuous-${VERSION}.tar.gz
