.. vim: wrap lbr spell

Wow, zero stars. Thanks a lot, my one friend who uses this still.

Impetuous
=========

This is time tracking software with a CLI.  (It might also be able to post time to Jira & Freshdesk.)

This is a Rust_ rewrite and successor to `a Python program also named Impetuous`__.

This program works very similar to that one.  But this rewrite adds some quality-of-life improvements around specifying dates & times, editing, and displaying information in the CLI.

It's also quite a bit more responsive to use; the Python implementation spent most of its time importing Python modules instead of actually tracking time.

    I tried out using impetuous-rs today, for no real reason, but it does seem a lot nicer than the Python version

    -- Rhakyr

__ https://gitlab.com/sqwishy/impetuous
.. _Rust: https://www.rust-lang.org/

Usage
-----

For more *usage*, or an impression of what the program is like, refer to the readme for the Python version linked above because I'm too lazy to write something comprehensive here.

To configure things, see `im example-config`.  On Linux, impetuous should respect XDG paths and look for configs in `~/.config/impetuous`.  Most of the settings go in `config.toml`, except secrets for posting time to some Jira or Freshdesk or whatever go in `secrets.toml`.

One important difference between this and the Python version the addition a some sort of annoying "tag" notation; which just looks like `:foo` or `:foo=bar` in your time entries.  This notation replaces the regular expression that the Python version used to match time entries for posting time. It's also used used to group time by topics in the `summary` command.

Installation
------------

I have `a project at copr.fedorainfracloud.org`__ that builds Fedora packages.

__ https://copr.fedorainfracloud.org/coprs/sqwishy/impetuous-rs/

Or you can compile it from source by running `cargo build --release` in the `cli/` directory or something.

(The Makefile is just to aid in creating source RPMs. Not compiling or installing.)

Licence
-------

This is licenced under Apache License, Version 2.0.
