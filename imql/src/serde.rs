use std::cell::RefCell;
use std::collections::BTreeMap;
use std::fmt;
use std::ops::{Deref, DerefMut};
use std::rc::Rc;

use serde::de::{
    self, DeserializeOwned, DeserializeSeed, Deserializer, IgnoredAny, MapAccess, Visitor,
};

pub trait Expected {
    fn deserialize_erased(
        &mut self,
        de: &mut dyn erased_serde::Deserializer,
    ) -> std::result::Result<(), erased_serde::Error>;
}

impl<T> Expected for Option<T>
where
    T: DeserializeOwned,
{
    fn deserialize_erased(
        &mut self,
        de: &mut dyn erased_serde::Deserializer,
    ) -> std::result::Result<(), erased_serde::Error> {
        self.replace(T::deserialize(de)?);
        Ok(())
    }
}

impl Expected for Box<dyn Expected> {
    fn deserialize_erased(
        &mut self,
        de: &mut dyn erased_serde::Deserializer,
    ) -> std::result::Result<(), erased_serde::Error> {
        self.as_mut().deserialize_erased(de)
    }
}

// seriously what the fuck?
impl<T: Expected + ?Sized> Expected for &mut T {
    fn deserialize_erased(
        &mut self,
        de: &mut dyn erased_serde::Deserializer,
    ) -> std::result::Result<(), erased_serde::Error> {
        (*self).deserialize_erased(de)
    }
}

impl<T> Expected for Rc<RefCell<T>>
where
    T: Expected,
{
    fn deserialize_erased(
        &mut self,
        de: &mut dyn erased_serde::Deserializer,
    ) -> std::result::Result<(), erased_serde::Error> {
        self.borrow_mut().deserialize_erased(de)
    }
}

pub trait Fulfillment<'a, K, V>
where
    K: AsRef<str> + Ord,
    V: Expected,
{
    type Error;

    fn fulfill(self, _: &mut Expectation<K, V>) -> Result<(), Self::Error>;
}

impl<'de, K, V, D> Fulfillment<'de, K, V> for D
where
    K: AsRef<str> + Ord + serde::Deserialize<'de> + fmt::Display,
    V: Expected,
    D: serde::Deserializer<'de>,
{
    type Error = <Self as serde::Deserializer<'de>>::Error;

    fn fulfill(self, e: &mut Expectation<K, V>) -> Result<(), Self::Error> {
        e.deserialize(self)
    }
}

pub struct Expectation<S, E>
where
    S: AsRef<str> + Ord,
    E: Expected,
{
    pub map: BTreeMap<S, E>,
}

impl<S, E> Default for Expectation<S, E>
where
    S: AsRef<str> + Ord,
    E: Expected,
{
    fn default() -> Self {
        Expectation {
            map: BTreeMap::new(),
        }
    }
}

impl<S, E> Expectation<S, E>
where
    S: AsRef<str> + Ord,
    E: Expected,
{
    pub fn new() -> Self {
        Expectation {
            map: BTreeMap::new(),
        }
    }

    /// This goes through the deserializer like a mapping. When we find a key that
    /// matches one of our own entries, we try to deserialize the Expected value. If we
    /// are successful, the item is remove from our own mapping as to not deserialize it
    /// again. If we see items in the deserializer that we didn't expect, they are
    /// ignored.
    ///
    /// Since we remove every Expected item we visit, we should be empty if we
    /// successfully found all our keys in the given deserializer.
    pub fn fulfill_<'de, D: Deserializer<'de>>(&mut self, de: D) -> Result<(), D::Error>
    where
        S: serde::Deserialize<'de> + fmt::Display,
    {
        self.deserialize(de)
    }

    pub fn fulfill<'a, F: Fulfillment<'a, S, E>>(&mut self, f: F) -> Result<(), F::Error> {
        f.fulfill(self)
    }
}

impl<S, E> Deref for Expectation<S, E>
where
    S: AsRef<str> + Ord,
    E: Expected,
{
    type Target = BTreeMap<S, E>;

    fn deref(&self) -> &Self::Target {
        &self.map
    }
}

impl<S, E> DerefMut for Expectation<S, E>
where
    S: AsRef<str> + Ord,
    E: Expected,
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.map
    }
}

impl<'de, S, E> DeserializeSeed<'de> for &mut Expectation<S, E>
where
    S: AsRef<str> + Ord + serde::Deserialize<'de> + fmt::Display,
    E: Expected,
{
    type Value = ();

    fn deserialize<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(self)
    }
}

impl<'de, S, E> Visitor<'de> for &mut Expectation<S, E>
where
    S: AsRef<str> + Ord + serde::Deserialize<'de> + fmt::Display,
    E: Expected,
{
    type Value = ();

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            "mapping of strings to values typed appropriately for the referencing query"
        )
    }

    fn visit_map<M>(self, mut map: M) -> Result<Self::Value, M::Error>
    where
        M: MapAccess<'de>,
    {
        while let Some(key) = map.next_key::<S>()? {
            if let Some(derp) = self.map.remove(&key) {
                map.next_value_seed(ExpectationSeed(key, derp))?;
            } else {
                map.next_value::<IgnoredAny>()?;
            }
        }
        Ok(())
    }
}

/// this is needed because I can't implement a foreign trait for a generic local trait?
/// ... that is
/// impl<T> foreign::Trait for T
///     where T: MyOwnGodDamnLocalTrait???
struct ExpectationSeed<S, E>(S, E);

impl<'de, S, E> DeserializeSeed<'de> for ExpectationSeed<S, E>
where
    S: fmt::Display,
    E: Expected,
{
    type Value = ();

    fn deserialize<D>(mut self, deserializer: D) -> Result<Self::Value, D::Error>
    where
        D: Deserializer<'de>,
    {
        let mut erased = erased_serde::Deserializer::erase(deserializer);
        self.1
            .deserialize_erased(&mut erased)
            .map_err(|e| de::Error::custom(format!("{}: {}", self.0, e)))
    }
}

#[cfg(test)]
mod tests {
    use std::collections::BTreeMap;

    use super::{Expectation, Expected};

    const JSON: &[u8] = br#"{ "stringy": "foo",
                              "numbery": 123,
                              "opt": true }"#;

    #[test]
    fn test() {
        let mut json = serde_json::Deserializer::from_slice(JSON);

        let mut stringy: Option<String> = None;
        let mut numbery: Option<i32> = None;
        let mut opt: Option<Option<bool>> = None;

        let mut map: BTreeMap<&str, &mut dyn Expected> = BTreeMap::new();
        map.insert("stringy", &mut stringy);
        map.insert("numbery", &mut numbery);
        map.insert("opt", &mut opt);
        Expectation { map }.fulfill(&mut json).unwrap();

        assert_eq!(stringy, Some("foo".to_owned()));
        assert_eq!(numbery, Some(123));
        assert_eq!(opt, Some(Some(true)));
    }
}
